package introspection;

import beans.ExampleBean;
import org.junit.jupiter.api.Test;
import type.Type;
import type.hardcoded.BeanVM_BeanType;
import type.hardcoded.JavaClassType;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ObjectHelperTest {

    @Test
    void isEqual() {
        Object[] objectArray = new Object[]{1, 2};
        Object[] objectArray2 = new Object[]{1, 2};
        Object[] objectArray3 = new Object[]{1, 5};
        Object object = 1;
        Object object2 = new Object();
        Object object3 = new Object();
        Object object4 = 1;

        assertTrue(ObjectHelper.isEqual(object, object4));
        assertFalse(ObjectHelper.isEqual(object2, object3));
        assertTrue(ObjectHelper.isEqual(objectArray, objectArray2));
        assertFalse(ObjectHelper.isEqual(objectArray, objectArray3));
    }

    @Test
    void isInstantiable() {
        assertTrue(ObjectHelper.isInstantiable(Object.class));
        assertTrue(ObjectHelper.isInstantiable(ExampleBean.class));
        assertTrue(ObjectHelper.isInstantiable(String.class));

        assertFalse(ObjectHelper.isInstantiable(List.class));
        assertFalse(ObjectHelper.isInstantiable(Type.class));
    }

    @Test
    void isValueAssignable() {
        assertTrue(ObjectHelper.isValueAssignable(4, Object.class));
        assertTrue(ObjectHelper.isValueAssignable(new ExampleBean(), ExampleBean.class));
        assertTrue(ObjectHelper.isValueAssignable("5", String.class));

        assertFalse(ObjectHelper.isValueAssignable(new int[]{}, List.class));
        assertFalse(ObjectHelper.isValueAssignable(new JavaClassType(Integer.class), BeanVM_BeanType.class));
    }
}