/*
 * @author apmishutkin@edu.hse.ru
 */

package introspection;

import beans.ExampleBean;
import org.junit.jupiter.api.Test;

import java.beans.BeanInfo;

import static org.junit.jupiter.api.Assertions.*;

class BeanHelperTest {
    @Test
    void getBeanInfo() {
        BeanInfo beanInfo = BeanHelper.getBeanInfo(ExampleBean.class);
        assertEquals(1, beanInfo.getPropertyDescriptors().length);
        assertEquals("a", beanInfo.getPropertyDescriptors()[0].getName());
    }
}