/*
 * @author apmishutkin@edu.hse.ru
 */

package introspection;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayHelperTest {

    @Test
    void cloneValueIfArray() {
        Object object = new Object();

        assertEquals(object, ArrayHelper.cloneValueIfArray(object));

        Object[] objectArray = new Object[]{1, 2};

        assertNotEquals(objectArray, ArrayHelper.cloneValueIfArray(objectArray));
        assertArrayEquals(objectArray, (Object[]) ArrayHelper.cloneValueIfArray(objectArray));
    }

    @Test
    void getArrayValueElement() {
        Object[] objectArray = new Object[]{1, 2};
        assertEquals(objectArray[0], ArrayHelper.getArrayValueElement(objectArray, 0));
        assertNotEquals(objectArray[1], ArrayHelper.getArrayValueElement(objectArray, 0));
    }

    @Test
    void setArrayValueElement() {
        Object[] objectArray = new Object[]{1, 2};

        ArrayHelper.setArrayValueElement(objectArray, 0, 2);
        ArrayHelper.setArrayValueElement(objectArray, 1, 1);

        assertEquals(2, objectArray[0]);
        assertNotEquals(2, objectArray[1]);
    }

    @Test
    void areArraysEquals() {
        Object[] objectArray = new Object[]{1, 2};
        Object[] objectArray2 = new Object[]{1, 2};
        Object[] objectArray3 = new Object[]{1, 5};
        Object[] objectArray4 = new Object[]{1, 2, 9};
        Object[] objectArray5 = null;

        assertTrue(ArrayHelper.areArraysEquals(objectArray, objectArray2));
        assertFalse(ArrayHelper.areArraysEquals(objectArray, objectArray3));
        assertFalse(ArrayHelper.areArraysEquals(objectArray, objectArray4));
        //noinspection ConstantConditions
        assertFalse(ArrayHelper.areArraysEquals(objectArray, objectArray5));
    }
}