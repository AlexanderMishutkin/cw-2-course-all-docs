/*
 * @author apmishutkin@edu.hse.ru
 */

package introspection;

import beans.ExampleBean;
import org.junit.jupiter.api.Test;
import properties.AccessManager;

import static org.junit.jupiter.api.Assertions.*;

class PropertyHelperTest {

    @Test
    void getPropertyDescriptors() {
        assertEquals(1, PropertyHelper.getPropertyDescriptors(ExampleBean.class).size());
        assertEquals("a", PropertyHelper.getPropertyDescriptors(ExampleBean.class).get(0).getName());
    }

    @Test
    void getPropertyValueClass() {
        assertEquals(int.class,
                PropertyHelper.getPropertyValueClass(PropertyHelper.getPropertyDescriptors(ExampleBean.class).get(0)));
    }

    @Test
    void getAccessLevel() {
        assertEquals(AccessManager.ALL_SCALAR,
                PropertyHelper.getAccessLevel(PropertyHelper.getPropertyDescriptors(ExampleBean.class).get(0)));
    }
}