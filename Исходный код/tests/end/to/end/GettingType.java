/*
 * @author apmishutkin@edu.hse.ru
 */

package end.to.end;

import beans.Bean;
import type.Type;

public class GettingType {
    public static class SomeBean_A extends Bean {
        public SomeBean_A() {
            //initPropertyValue("a", 2);
        }

        public void setA(int a) {
            this.a = a;
        }

        private int a = 0;

        public int getA() {
            return a;
        }

        public void incr() {
            sudoSetPropertyValue(0, (int) sudoGetPropertyValue("a") + 1);
        }
    }

    public static class SomeBean_B extends Bean {
        public void setKek(SomeBean_A kek) {
            this.kek = kek;
        }

        private SomeBean_A kek = null;

        public SomeBean_B() {
            //initPropertyValue(0, new SomeBean_A());
        }

        public SomeBean_A getKek() {
            return kek;
        }
    }

    @org.junit.jupiter.api.Test
    public void getTypeOfSomeBean() {
        Type type = Type.forClass(SomeBean_B.class);
        Bean b = new SomeBean_B();
        b.addPropertyChangeListener(evt -> System.out.println(evt.getOldValue().toString() + " -> " +
                (evt.getNewValue() == null ? null : evt.getNewValue().toString())));
        b.setPropertyValue("kek", null);
        Bean a = (Bean) b.getPropertyValue(0);
    }
}
