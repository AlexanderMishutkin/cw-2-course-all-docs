/*
 * @author apmishutkin@edu.hse.ru
 */

package properties;

import beans.ExampleBean;
import engine.BeanVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import type.Type;

import static org.junit.jupiter.api.Assertions.*;

class PropertiesInfoProviderTest {

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
    }

    @Test
    void getPropertiesInfo() {
        PropertiesInfoProvider propertiesInfoProvider = new PropertiesInfoProvider(ExampleBean.class);
        assertEquals(1, propertiesInfoProvider.getPropertiesInfo().length);
        assertEquals("a", propertiesInfoProvider.getPropertiesInfo()[0].getName());
        assertEquals(Type.forClass(int.class), propertiesInfoProvider.getPropertiesInfo()[0].getType());
    }
}