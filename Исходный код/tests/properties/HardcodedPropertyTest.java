/*
 * @author apmishutkin@edu.hse.ru
 */

package properties;

import beans.ExampleBean;
import beans.ExampleBeanWithArrProp;
import beans.HardcodedBeanFunctionalProvider;
import engine.BeanVM;
import exceptions.BeanVM_AccessLevelException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import type.hardcoded.HardcodedType;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EnumSet;

import static org.junit.jupiter.api.Assertions.*;

class HardcodedPropertyTest {
    HardcodedProperty hardcodedProperty;
    HardcodedBeanFunctionalProvider stub;
    ExampleBean protoStub;

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
        protoStub = new ExampleBean();
        stub = new HardcodedBeanFunctionalProvider((HardcodedType) protoStub.getType(), protoStub);
        hardcodedProperty =
                HardcodedProperty.fromPropertyInfo(stub, new PropertiesInfoProvider(protoStub).getPropertiesInfo()[0]);
    }

    @Test
    void setAccessLevel() {
        hardcodedProperty.setValue(3);
        hardcodedProperty.setAccessLevel(AccessManager.readable());
        assertThrows(BeanVM_AccessLevelException.class, () -> hardcodedProperty.setValue(3));
    }

    @Test
    void isIndexed() {
        assertFalse(hardcodedProperty.isIndexed());
        hardcodedProperty =
                HardcodedProperty.fromPropertyInfo(stub, new PropertiesInfoProvider(new ExampleBeanWithArrProp())
                        .getPropertiesInfo()[1]);
        assertTrue(hardcodedProperty.isIndexed());
    }

    @Test
    void addPropertyChangeListener() {
        final boolean[] property_called = {false};
        hardcodedProperty.addPropertyChangeListener(evt -> property_called[0] = true);

        hardcodedProperty.setValue(1);
        assertTrue(property_called[0]);
    }

    @Test
    void removePropertyChangeListener() {
        var listener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                fail("Removed listener called");
            }
        };
        hardcodedProperty.addPropertyChangeListener(listener);

        final boolean[] property_called = {false};
        hardcodedProperty.addPropertyChangeListener(evt -> property_called[0] = true);

        hardcodedProperty.removePropertyChangeListener(listener);
        hardcodedProperty.setValue(1);
        assertTrue(property_called[0]);
    }

    @Test
    void setValue() {
        assertEquals(0, hardcodedProperty.getValue());
        hardcodedProperty.setValue(1);
        assertEquals(1, hardcodedProperty.getValue());
    }

    @Test
    void setValueIndexed() {
        hardcodedProperty =
                HardcodedProperty.fromPropertyInfo(stub, new PropertiesInfoProvider(new ExampleBeanWithArrProp())
                        .getPropertiesInfo()[1]);
        hardcodedProperty.setValue(new String[]{"lol"});
        assertEquals("lol", hardcodedProperty.getValue(0));
        hardcodedProperty.setValue(0, "лол");
        assertEquals("лол", hardcodedProperty.getValue(0));
    }

    @Test
    void getAccessLevel() {
        assertEquals("rwb--", AccessManager.toString(hardcodedProperty.getAccessLevel()));
        hardcodedProperty =
                HardcodedProperty.fromPropertyInfo(stub, new PropertiesInfoProvider(new ExampleBeanWithArrProp())
                        .getPropertiesInfo()[1]);
        assertEquals("rwbRW", AccessManager.toString(hardcodedProperty.getAccessLevel()));
    }

    @Test
    void getName() {
        assertEquals("a", hardcodedProperty.getName());
        hardcodedProperty =
                HardcodedProperty.fromPropertyInfo(stub, new PropertiesInfoProvider(new ExampleBeanWithArrProp())
                        .getPropertiesInfo()[1]);
        assertEquals("b", hardcodedProperty.getName());
    }

    @Test
    void logIn() {
        hardcodedProperty.setAccessLevel(EnumSet.noneOf(AccessManager.AccessRight.class));
        assertThrows(BeanVM_AccessLevelException.class, hardcodedProperty::getValue);
        assertEquals(0, hardcodedProperty.logIn(stub).sudoGetValue());
    }
}