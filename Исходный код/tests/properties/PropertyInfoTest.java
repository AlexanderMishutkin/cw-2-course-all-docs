/*
 * @author apmishutkin@edu.hse.ru
 */

package properties;

import beans.ExampleBean;
import engine.BeanVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import type.Type;

import static org.junit.jupiter.api.Assertions.*;

class PropertyInfoTest {
    PropertyInfo propertyInfo;

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
        propertyInfo = new PropertyInfo("prop", Type.forClass(ExampleBean.class), AccessManager.ALL_VECTOR);
    }

    @Test
    void getType() {
        assertEquals(Type.forClass(ExampleBean.class), propertyInfo.getType());
    }

    @Test
    void getAccessLevel() {
        assertEquals(AccessManager.ALL_VECTOR, propertyInfo.getAccessLevel());
    }

    @Test
    void getName() {
        assertEquals("prop", propertyInfo.getName());
    }
}