/*
 * @author apmishutkin@edu.hse.ru
 */

package properties;

import org.junit.jupiter.api.Test;

import java.util.EnumSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AccessManagerTest {

    @org.junit.jupiter.api.Test
    void fromBitmask() {
        assertEquals(EnumSet.of(AccessManager.AccessRight.READABLE, AccessManager.AccessRight.INDEXED_READABLE),
                AccessManager.fromBitmask(0b10010));
        assertEquals(EnumSet.of(AccessManager.AccessRight.WRITABLE, AccessManager.AccessRight.BINDABLE),
                AccessManager.fromBitmask(0b01100));
        assertEquals(EnumSet.noneOf(AccessManager.AccessRight.class),
                AccessManager.fromBitmask(0));
        assertEquals(AccessManager.ALL_VECTOR,
                AccessManager.fromBitmask(0b11111));
    }

    @org.junit.jupiter.api.Test
    void fromFlags() {
        assertEquals(EnumSet.of(AccessManager.AccessRight.READABLE, AccessManager.AccessRight.INDEXED_READABLE),
                AccessManager.fromFlags(true, false, false, true, false));
        assertEquals(EnumSet.of(AccessManager.AccessRight.WRITABLE, AccessManager.AccessRight.BINDABLE),
                AccessManager.fromFlags(false, true, true, false, false));
        assertEquals(EnumSet.noneOf(AccessManager.AccessRight.class),
                AccessManager.fromFlags(false, false, false, false, false));
        assertEquals(AccessManager.ALL_VECTOR,
                AccessManager.fromFlags(true, true, true, true, true));
    }

    @org.junit.jupiter.api.Test
    void fromString() {
        assertEquals(EnumSet.of(AccessManager.AccessRight.READABLE, AccessManager.AccessRight.INDEXED_READABLE),
                AccessManager.fromString("rR"));
        assertEquals(EnumSet.of(AccessManager.AccessRight.WRITABLE, AccessManager.AccessRight.BINDABLE),
                AccessManager.fromString("wb"));
        assertEquals(EnumSet.noneOf(AccessManager.AccessRight.class),
                AccessManager.fromString("kek lol"));
        assertEquals(AccessManager.ALL_VECTOR,
                AccessManager.fromString("rwbRW"));
    }

    @Test
    void testToString() {
        assertEquals("r--R-",
                AccessManager.toString(
                        EnumSet.of(AccessManager.AccessRight.READABLE, AccessManager.AccessRight.INDEXED_READABLE)));
        assertEquals("-wb--",
                AccessManager.toString(
                        EnumSet.of(AccessManager.AccessRight.WRITABLE, AccessManager.AccessRight.BINDABLE)));
        assertEquals("-----",
                AccessManager.toString(
                        EnumSet.noneOf(AccessManager.AccessRight.class)));
        assertEquals("rwbRW",
                AccessManager.toString(
                        AccessManager.ALL_VECTOR));
    }

    @Test
    void toBitmask() {
        assertEquals(0b10010,
                AccessManager.toBitmask(
                        EnumSet.of(AccessManager.AccessRight.READABLE, AccessManager.AccessRight.INDEXED_READABLE)));
        assertEquals(0b01100,
                AccessManager.toBitmask(
                        EnumSet.of(AccessManager.AccessRight.WRITABLE, AccessManager.AccessRight.BINDABLE)));
        assertEquals(0,
                AccessManager.toBitmask(
                        EnumSet.noneOf(AccessManager.AccessRight.class)));
        assertEquals(0b11111,
                AccessManager.toBitmask(
                        AccessManager.ALL_VECTOR));
    }
}