/*
 * @author apmishutkin@edu.hse.ru
 */

package beans;

import engine.BeanVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import type.Type;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.junit.jupiter.api.Assertions.*;

public class BeanTest {
    ExampleBean exampleBean;
    ExampleBeanWithArrProp arrBean;

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
        exampleBean = new ExampleBean();
        arrBean = new ExampleBeanWithArrProp();
    }

    @Test
    void getType() {
        assertEquals(Type.forClass(ExampleBean.class), exampleBean.getType());
        assertEquals(Type.forClass(ExampleBeanWithArrProp.class), arrBean.getType());
    }

    @Test
    void propertyValue() {
        arrBean.setPropertyValue("a", 4);
        arrBean.setPropertyValue("b", 1, "5");
        assertEquals("5", arrBean.getPropertyValue("b", 1));
        assertEquals(4, arrBean.getPropertyValue("a"));
    }

    public static class ReadableBean extends ExampleBeanWithArrProp {
        private int c = 2;
        private String[] d;

        public ReadableBean() {
            initPropertyValue("d", new String[]{"t", "t"});
        }

        public int getC() {
            return c;
        }

        private void setC(int c) {
            this.c = c;
        }

        public String[] getD() {
            return d;
        }

        private void setD(String[] d) {
            this.d = d;
        }
    }

    @Test
    void sudoPropertyValue() {
        ReadableBean readableBean = new ReadableBean();

        readableBean.sudoSetPropertyValue("c", 4);
        readableBean.sudoSetPropertyValue("d", 1, "5");
        assertEquals(4, readableBean.sudoGetPropertyValue("c"));
        assertEquals("5", readableBean.sudoGetPropertyValue("d", 1));
    }

    @Test
    void addPropertyChangeListener() {
        final boolean[] property_called = {false, false, false};
        exampleBean.addPropertyChangeListener(evt -> property_called[0] = true);
        exampleBean.addPropertyChangeListener(0, evt -> property_called[1] = true);
        exampleBean.sudoAddPropertyChangeListener(0, evt -> property_called[2] = true);

        exampleBean.setPropertyValue(0, 1);
        assertTrue(property_called[0]);
        assertTrue(property_called[1]);
        assertTrue(property_called[2]);
    }

    @Test
    void removePropertyChangeListener() {
        var listener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                fail("Removed listener called");
            }
        };
        exampleBean.addPropertyChangeListener(listener);

        final boolean[] property_called = {false};
        exampleBean.addPropertyChangeListener(evt -> property_called[0] = true);

        exampleBean.removePropertyChangeListener(listener);
        exampleBean.setPropertyValue(0, 1);
        assertTrue(property_called[0]);
    }
}