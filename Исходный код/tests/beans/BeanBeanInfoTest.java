/*
 * @author apmishutkin@edu.hse.ru
 */

package beans;

import introspection.PropertyHelper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BeanBeanInfoTest {

    @Test
    void getPropertyDescriptors() {
        assertTrue(PropertyHelper.getPropertyDescriptors(Bean.class).isEmpty());
    }
}