/*
 * @author apmishutkin@edu.hse.ru
 */

package beans;

/**
 * Simple Bean with two props:
 * integer property "a" with default value 2
 * String[] property with default value {"lol", "kek"}
 */
public class ExampleBeanWithArrProp extends ExampleBean {
    private String[] b = new String[]{};

    public ExampleBeanWithArrProp() {
        initPropertyValue("b", new String[]{"lol", "kek"});
    }

    public String[] getB() {
        return b;
    }

    public void setB(String[] arr) {
        this.b = arr;
    }

    public String getB(int index) {
        return b[index];
    }

    public void setB(int index, String value) {
        this.b[index] = value;
    }
}
