/*
 * @author apmishutkin@edu.hse.ru
 */

package beans;

import type.Type;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Simple Bean with one integer property "a" with default value 2
 */
public class ExampleBean extends Bean {
    public ExampleBean() {
        initPropertyValue("a", 2);
    }

    public void setA(int a) {
        this.a = a;
    }

    private int a = 0;

    public int getA() {
        return a;
    }

    public void incr() {
        sudoSetPropertyValue(0, (int) sudoGetPropertyValue("a") + 1);
    }
}

class A {
    static public void foo() {
        ExampleBeanWithArrProp exampleBean = new ExampleBeanWithArrProp();

        System.out.println(exampleBean.getPropertyValue("a")); // Вывод значения свойства с именем "а"
        System.out.println(exampleBean.getPropertyValue(1)); // Вывод значения первого свойства
        exampleBean.setPropertyValue("b", 0, "val"); // Изменение значения первого свойства с именем "b" на "val"
        exampleBean.setPropertyValue(1, 1, "val2"); // Изменение значения второго элемента первого свойства на "val2"

        PropertyChangeListener propertyChangeListener = evt -> System.out.println("Hello"); // Создание объекта подписчика
        exampleBean.addPropertyChangeListener("b", propertyChangeListener); // Подписывание подписчика на свойство с именем "b"
        exampleBean.removePropertyChangeListener("b", propertyChangeListener); // Отписывание подписчика от свойства с именем "b"
        exampleBean.addPropertyChangeListener(1, propertyChangeListener); // Подписывание подписчика на первое свойство
        exampleBean.removePropertyChangeListener(1, propertyChangeListener); // Отписывание подписчика от первого свойства

        Type type1 = Type.forClass(exampleBean.getClass());
        Type type = Type.forClass(ExampleBean.class);

        // Получения некоторых базовых свойств типа
        type.getName(); // -> ExampleBean
        type.getDefaultValue(); // -> new ExampleBean()

        // Узнать конкретный подтип
        type.isBeanArrayType(); // -> False
        type.isArrayType(); // -> False
        type.isBeanType(); // -> True

        // Проверка типа при присваивании
        type.isValueAssignable(new ExampleBeanWithArrProp()); // -> True
        type.isValueAssignable(4); // -> False

        System.out.println(type.toStringForm());
        System.out.println(exampleBean.toStringForm());
    }
}