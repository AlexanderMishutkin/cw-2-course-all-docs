/*
 * @author apmishutkin@edu.hse.ru
 */

package type.loader;

import beans.ExampleBean;
import engine.BeanVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TypeLoaderTest {
    @BeforeEach
    void beforeEach() {
        BeanVM.clearAllCaches();
    }

    @Test
    void forClass() {
        assertEquals(int.class.getName(), TypeLoader.forClass(int.class).getTypeFromClass(int.class).getName());
        assertEquals(ExampleBean.class.getName(),
                TypeLoader.forClass(ExampleBean.class).getTypeFromClass(ExampleBean.class).getName());
    }

    @Test
    void registerDefaultValue() {
        ExampleBean exampleBean = new ExampleBean();
        TypeLoader typeLoader = TypeLoader.forClass(ExampleBean.class);
        TypeLoader.registerDefaultValue(ExampleBean.class, exampleBean);
        assertEquals(exampleBean, typeLoader.getRegisteredDefaultValue(ExampleBean.class));
    }

    @Test
    void getTypeFromClass() {
        assertEquals(int.class.getName(), TypeLoader.forClass(int.class).getTypeFromClass(int.class).getName());
        assertEquals(ExampleBean.class.getName(),
                TypeLoader.forClass(ExampleBean.class).getTypeFromClass(ExampleBean.class).getName());
    }

    @Test
    void loadType() {
        assertEquals(int.class.getName(), TypeLoader.forClass(int.class).loadType(int.class).getName());
        assertEquals(ExampleBean.class.getName(),
                TypeLoader.forClass(ExampleBean.class).loadType(ExampleBean.class).getName());
    }

    @Test
    void getParent() {
        assertNotEquals(TypeLoader.forClass(int.class), TypeLoader.forClass(ExampleBean.class));
        assertEquals(TypeLoader.forClass(int.class).getTypeFromClass(int.class),
                TypeLoader.forClass(ExampleBean.class).getTypeFromClass(int.class));
    }

    @Test
    void getRegisteredDefaultValue() {
        ExampleBean exampleBean = new ExampleBean();
        TypeLoader typeLoader = TypeLoader.forClass(ExampleBean.class);
        TypeLoader.registerDefaultValue(ExampleBean.class, exampleBean);
        assertEquals(exampleBean, typeLoader.getRegisteredDefaultValue(ExampleBean.class));
    }
}