/*
 * @author apmishutkin@edu.hse.ru
 */

package type;

import beans.ExampleBean;
import engine.BeanVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import type.hardcoded.BeanVM_BeanType;
import type.hardcoded.HardcodedType;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class TypeTest {

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
    }

    @Test
    void forClass() {
        ExampleBean bean = new ExampleBean();
        Type type = Type.forClass(bean.getClass());

        assertTrue(type instanceof HardcodedType);
        assertTrue(type instanceof BeanVM_BeanType);

        assertEquals(bean.getClass().getName(), type.getName());
    }

    @Test
    void getType() {
        ExampleBean bean = new ExampleBean();
        Type type = Type.forClass(bean.getClass());
        assertEquals(Type.forClass(Type.class), type.getType());
        assertEquals(Type.forClass(Type.class), type.getType().getType());
        assertEquals(Type.forClass(Type.class), type.getType().getType().getType());
        assertEquals(Type.forClass(Type.class), type.getType().getType().getType().getType());
    }

    @Test
    void getDefaultValue() {
        assertEquals(0, Type.forClass(int.class).getDefaultValue());
        assertEquals("", Type.forClass(String.class).getDefaultValue());
        assertNull(Type.forClass(Object.class).getDefaultValue());
        assertNull(Type.forClass(ExampleBean.class).getDefaultValue());
    }

    @Test
    void getName() {
        ExampleBean bean = new ExampleBean();
        Type type = Type.forClass(bean.getClass());
        assertEquals(bean.getClass().getName(), type.getName());
    }

    @Test
    void isBeanType() {
        assertTrue(Type.forClass(ExampleBean.class).isBeanType());
        assertFalse(Type.forClass(Object.class).isBeanType());
    }

    @Test
    void isBeanArrayType() {
        assertTrue(Type.forClass(ExampleBean[].class).isBeanArrayType());
        assertFalse(Type.forClass(Object[].class).isBeanArrayType());
    }

    @Test
    void isArrayType() {
        assertTrue(Type.forClass(Object[].class).isArrayType());
        assertFalse(Type.forClass(ArrayList.class).isArrayType());
    }

    @Test
    void isValueAssignable() {
        assertTrue(Type.forClass(int.class).isValueAssignable(1));
        assertFalse(Type.forClass(int.class).isValueAssignable(new ExampleBean()));

        assertTrue(Type.forClass(ExampleBean.class).isValueAssignable(new ExampleBean()));
        assertFalse(Type.forClass(ExampleBean.class).isValueAssignable(1));
    }

    @Test
    void testGetDefaultValue() {
        assertEquals(0, Type.getDefaultValue(int.class));
        assertEquals("", Type.getDefaultValue(String.class));
        assertNull(Type.getDefaultValue(Object.class));
        assertNull(Type.getDefaultValue(ExampleBean.class));
    }
}