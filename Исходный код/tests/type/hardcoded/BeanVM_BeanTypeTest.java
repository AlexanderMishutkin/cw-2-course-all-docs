/*
 * @author apmishutkin@edu.hse.ru
 */

package type.hardcoded;

import beans.ExampleBean;
import engine.BeanVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import type.Type;
import type.TypeTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BeanVM_BeanTypeTest extends TypeTest {

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
    }

    @Test
    void getBeanFunctionalProvider() {
        BeanVM_BeanType beanVM_beanType = (BeanVM_BeanType) Type.forClass(ExampleBean.class);
        ExampleBean bean = new ExampleBean(); // getBeanFunctionalProvider was used in Bean constructor
        assertEquals(2, bean.getPropertyValue("a"));
        bean.setPropertyValue("a", 4);
        assertEquals(4, bean.getPropertyValue("a"));
    }
}