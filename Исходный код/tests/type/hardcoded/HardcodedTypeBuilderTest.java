/*
 * @author apmishutkin@edu.hse.ru
 */

package type.hardcoded;

import beans.ExampleBean;
import engine.BeanVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import type.Type;

import static org.junit.jupiter.api.Assertions.*;

class HardcodedTypeBuilderTest {

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
    }

    @Test
    void getHardcodedType() {
        HardcodedTypeBuilder hardcodedTypeBuilder = new HardcodedTypeBuilder(ExampleBean.class);
        assertThrows(IllegalStateException.class, hardcodedTypeBuilder::getHardcodedType);
        assertThrows(IllegalStateException.class, () -> hardcodedTypeBuilder.getHardcodedType(false));
        Type[] types = new Type[]{null};
        assertDoesNotThrow(() -> {
            types[0] = hardcodedTypeBuilder.getHardcodedType(true);
        });
        hardcodedTypeBuilder.create();
        assertEquals(types[0], hardcodedTypeBuilder.getHardcodedType());
        assertEquals(types[0], hardcodedTypeBuilder.getHardcodedType(true));
        assertEquals(types[0], hardcodedTypeBuilder.getHardcodedType(false));
        assertEquals(ExampleBean.class, hardcodedTypeBuilder.getHardcodedType(false).getImplementationClass());
    }
}