/*
 * @author apmishutkin@edu.hse.ru
 */

package type.hardcoded;

import beans.Bean;
import beans.ExampleBean;
import engine.BeanVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import type.Type;
import type.TypeTest;

import static org.junit.jupiter.api.Assertions.*;

class HardcodedTypeTest extends TypeTest {

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
    }

    @Test
    void getImplementationClass() {
        assertEquals(ExampleBean.class,
                ((HardcodedType) HardcodedType.forClass(ExampleBean.class)).getImplementationClass());
    }

    @Test
    void getSuperType() {
        assertEquals(Type.forClass(Bean.class),
                ((HardcodedType) HardcodedType.forClass(ExampleBean.class)).getSuperType());
    }
}