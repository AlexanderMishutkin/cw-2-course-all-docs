/*
 * @author apmishutkin@edu.hse.ru
 */

package type.hardcoded;

import beans.Bean;
import beans.ExampleBean;
import engine.BeanVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import type.Type;
import type.TypeTest;

import static org.junit.jupiter.api.Assertions.*;

class JavaArrayTypeTest extends TypeTest {

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
    }

    @Test
    void getElementType() {
        assertEquals(Type.forClass(int.class), ((JavaArrayType)Type.forClass(int[].class)).getElementType());
        assertEquals(Type.forClass(ExampleBean.class),
                ((JavaArrayType)Type.forClass(ExampleBean[].class)).getElementType());
        assertNotEquals(Type.forClass(Bean.class),
                ((JavaArrayType)Type.forClass(ExampleBean[].class)).getElementType());
    }
}