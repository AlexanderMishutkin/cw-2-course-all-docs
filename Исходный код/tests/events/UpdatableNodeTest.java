/*
 * @author apmishutkin@edu.hse.ru
 */

package events;

import engine.BeanVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UpdatableNodeTest {
    UpdatableNode child;
    UpdatableNode parent;

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
        child = new UpdatableNode(2);
        parent = new UpdatableNode(1, List.of(child));
    }

    @Test
    void hasChildren() {
        assertTrue(parent.hasChildren());
        assertFalse(child.hasChildren());
    }

    @Test
    void isMyDescendant() {
        assertTrue(parent.isMyDescendant(2));
        assertTrue(parent.isMyDescendant(1));
        assertFalse(child.isMyDescendant(1));
    }

    @Test
    void isMyAncestor() {
        assertFalse(parent.isMyAncestor(child));
        assertTrue(parent.isMyAncestor(parent));
        assertTrue(child.isMyAncestor(parent));
    }

    @Test
    void update() {
        child.change();
        assertTrue(child.isChanged());
        assertTrue(parent.isChanged());
        parent.update();
        assertFalse(parent.isChanged());
        assertTrue(child.isChanged()); // Why I do not update it?
    }
}