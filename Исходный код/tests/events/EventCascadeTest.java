/*
 * @author apmishutkin@edu.hse.ru
 */

package events;

import beans.ExampleBean;
import beans.HardcodedBeanFunctionalProvider;
import engine.BeanVM;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import type.hardcoded.HardcodedType;

class EventCascadeTest {
    ExampleBean exampleBean;
    HardcodedType exampleBeanType;
    HardcodedBeanFunctionalProvider exampleBeanAPI;

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
        exampleBean = new ExampleBean();
        exampleBeanType = (HardcodedType) exampleBean.getType();
        exampleBeanAPI = new HardcodedBeanFunctionalProvider(exampleBeanType, exampleBean);
    }

    @Test
    void registerRootNodeToUpdate() {
        exampleBeanAPI.change();
    }

    @Test
    void eventDelivered() {
        exampleBeanAPI.change();
        assertTrue(exampleBeanAPI.isChanged());
        exampleBeanAPI.setPropertyValue(0, 1);
        assertFalse(exampleBeanAPI.isChanged());
    }
}