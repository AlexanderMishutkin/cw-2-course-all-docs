/*
 * @author apmishutkin@edu.hse.ru
 */

package log;

import beans.Bean;
import beans.BeanTest;
import beans.ExampleBean;
import beans.ExampleBeanWithArrProp;
import engine.BeanVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import type.Type;

public class LoggerTest {

    @BeforeEach
    void setUp() {
        BeanVM.clearAllCaches();
    }

    @Test
    void typeToStringForm() {
        System.out.println("Type to string:");
        System.out.println(Logger.toStringForm(Type.forClass(ExampleBean.class)));
        System.out.println(Logger.toStringForm(Type.forClass(ExampleBeanWithArrProp.class)));
        System.out.println(Logger.toStringForm(Type.forClass(BeanTest.ReadableBean.class)));
    }

    public static class SpecialBean extends Bean {
        private ExampleBean bean;

        public SpecialBean() {
            initPropertyValue(0, new ExampleBean());
        }

        public ExampleBean getBean() {
            return bean;
        }
    }

    public static class SpecialArrBean extends Bean {
        private ExampleBean[] beans;
        private ExampleBean[] beans2;
        private ExampleBean[] beans3;

        public SpecialArrBean() {
            initPropertyValue(0, new ExampleBean[]{new ExampleBean(), new ExampleBean()});
            initPropertyValue(2, null);
        }

        public ExampleBean[] getBeans() {
            return beans;
        }

        public ExampleBean[] getBeans2() {
            return beans2;
        }

        public ExampleBean[] getBeans3() {
            return beans3;
        }
    }

    @Test
    void beanToStringForm() {
        System.out.println("Simple bean to string:");
        System.out.println("------------------------------------------------");
        System.out.println(Logger.toStringForm(new ExampleBean()));
        System.out.println(Logger.toStringForm(new ExampleBeanWithArrProp()));
        System.out.println(Logger.toStringForm(new BeanTest.ReadableBean()));

        System.out.println("################################################");
        System.out.println("Complicated bean to string:");
        System.out.println("------------------------------------------------");
        System.out.println(Logger.toStringForm(new SpecialBean()));
        System.out.println(Logger.toStringForm(new SpecialArrBean()));
    }
}