/*
 * @author apmishutkin@edu.hse.ru
 */

package sensetive.memory;

import org.junit.jupiter.api.Test;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.junit.jupiter.api.Assertions.*;

class VariableTest {

    @Test
    void addPropertyChangeListener() {
        Variable variable = new Variable(new Object[]{"a", "kek"});

        final boolean[] property_called = {false};
        variable.addPropertyChangeListener(evt -> {
            property_called[0] = true;
            assertTrue(evt instanceof IndexedPropertyChangeEvent);
            assertEquals(1, ((IndexedPropertyChangeEvent) evt).getIndex());
        });

        variable.setValue(1, 1);
        assertTrue(property_called[0]);
    }

    @Test
    void removePropertyChangeListener() {
        Variable variable = new Variable(new Object[]{"a", "kek"});

        var listener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                fail("Removed listener called");
            }
        };
        variable.addPropertyChangeListener(listener);

        final boolean[] property_called = {false};
        variable.addPropertyChangeListener(evt -> {
            property_called[0] = true;
            assertTrue(evt instanceof IndexedPropertyChangeEvent);
            assertEquals(1, ((IndexedPropertyChangeEvent) evt).getIndex());
        });

        variable.removePropertyChangeListener(listener);
        variable.setValue(1, 1);
        assertTrue(property_called[0]);
    }

    @Test
    void getValue() {
        Variable variable = new Variable("kek");
        assertEquals("kek", variable.getValue());
    }

    @Test
    void testGetValue() {
        Variable variable = new Variable(new String[]{"a", "kek"});
        assertEquals("kek", variable.getValue(1));
    }

    @Test
    void setValue() {
        Variable variable = new Variable("kek");
        assertEquals("kek", variable.getValue());
        variable.setValue(1);
        assertEquals(1, variable.getValue());
    }

    @Test
    void testSetValue() {
        Variable variable = new Variable(new Object[]{"a", "kek"});
        assertEquals("kek", variable.getValue(1));
        variable.setValue(1, 1);
        assertEquals(1, variable.getValue(1));
    }
}