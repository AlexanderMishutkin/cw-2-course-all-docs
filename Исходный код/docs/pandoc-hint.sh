#!/bin/bash
shopt -s globstar
for f in html/**/*.html; do
  pandoc "$f" -s -o "docx/$(echo "$f" | tr /._ -).docx"
done
