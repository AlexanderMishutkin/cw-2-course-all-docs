/*
 * @author apmishutkin@edu.hse.ru
 */

package properties;

import beans.BeanFunctionalProvider;
import beans.HardcodedBeanFunctionalProvider;
import exceptions.BeanVM_Exception;
import exceptions.BeanVM_TypeException;
import sensetive.memory.Variable;
import type.hardcoded.HardcodedType;
import type.hardcoded.JavaArrayType;
import type.Type;

import java.beans.PropertyChangeListener;
import java.util.EnumSet;

/**
 * Property of hardcoded type
 */
public class HardcodedProperty extends Property {
    private final HardcodedType type;
    private EnumSet<AccessManager.AccessRight> accessLevel;
    private final String name;
    private final Variable variable;

    static public HardcodedProperty fromPropertyInfo(BeanFunctionalProvider parent, PropertyInfo propertyInfo) {
        Variable variable = new Variable(propertyInfo.getType().getDefaultValue());
        return new HardcodedProperty(parent, propertyInfo, variable);
    }

    /**
     * @param parent       bean (it's functional part) to call feedback for property change events
     * @param propertyInfo to provide type and other for creating property
     */
    protected HardcodedProperty(BeanFunctionalProvider parent, PropertyInfo propertyInfo, Variable variable) {
        super(parent, variable);
        this.type = (HardcodedType) propertyInfo.getType();
        this.name = propertyInfo.getName();
        this.variable = variable;
        variable.addPropertyChangeListener(this);
        this.accessLevel = propertyInfo.getAccessLevel();
        sudoSetAccessLevel(propertyInfo.getAccessLevel().clone());
    }

    /**
     * @return level of access to property
     */
    @Override
    public EnumSet<AccessManager.AccessRight> getAccessLevel() {
        return accessLevel.clone();
    }

    /**
     * Force set of new access level
     *
     * @param newAccessLevel to set
     */
    @Override
    protected void sudoSetAccessLevel(EnumSet<AccessManager.AccessRight> newAccessLevel) {
        accessLevel = newAccessLevel;
    }

    /**
     * @return property name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @return type of property's value
     */
    @Override
    public HardcodedType getValueType() {
        return type;
    }

    /**
     * Force bind listener
     *
     * @param listener to bind
     */
    @Override
    protected void sudoAddPropertyChangeListener(PropertyChangeListener listener) {
        variable.addPropertyChangeListener(listener);
    }

    /**
     * Force remove listener
     *
     * @param listener to unbind
     */
    @Override
    protected void sudoRemovePropertyChangeListener(PropertyChangeListener listener) {
        variable.removePropertyChangeListener(listener);
    }

    /**
     * Force set value
     *
     * @param newValue new value to set
     */
    @Override
    protected void sudoSetValue(Object newValue) {
        if (getValueType().isValueAssignable(newValue)) {
            variable.setValue(newValue);
        } else {
            throw new BeanVM_TypeException(getName(), getValueType(),
                    newValue == null ? null : Type.forClass(newValue.getClass()));
        }
    }

    /**
     * Force get value
     *
     * @return new value to get
     */
    @Override
    protected Object sudoGetValue() {
        return variable.getValue();
    }

    /**
     * Force set element's value
     *
     * @param index    to find element
     * @param newValue to set
     */
    @Override
    protected void sudoSetValue(int index, Object newValue) {
        Type elementType = ((JavaArrayType) getValueType()).getElementType();
        if (elementType.isValueAssignable(newValue)) {
            variable.setValue(index, newValue);
        } else {
            throw new BeanVM_TypeException(getName(), getValueType(), Type.forClass(newValue.getClass()));
        }
    }

    /**
     * Force get element's value
     *
     * @param index to find element
     * @return element's value
     */
    @Override
    protected Object sudoGetValue(int index) {
        return variable.getValue(index);
    }

    /**
     * The hack I'm most proud for. It is needed to give access to sudo-methods from beans in outer packages
     *
     * @param owner bean owner of property
     * @return object that can execute sudo-commands on property
     * @throws BeanVM_Exception if it is not owner's property
     */
    public SuperUser logIn(HardcodedBeanFunctionalProvider owner) {
        if (owner.equals(getParent())) {
            return new SuperUser();
        }
        throw new BeanVM_Exception("HardcodedBeanFunctionalProvider can sudo change only his field");
    }

    /**
     * Allows to make no-access-level-check operations, but can be created only by property or it's owner
     */
    public class SuperUser {
        private SuperUser() {

        }

        /**
         * Force bind listener
         *
         * @param listener to bind
         */
        public void sudoAddPropertyChangeListener(PropertyChangeListener listener) {
            HardcodedProperty.this.sudoAddPropertyChangeListener(listener);
        }

        /**
         * Force remove listener
         *
         * @param listener to unbind
         */
        public void sudoRemovePropertyChangeListener(PropertyChangeListener listener) {
            HardcodedProperty.this.sudoRemovePropertyChangeListener(listener);
        }

        /**
         * Force set value
         *
         * @param newValue new value to set
         */
        public void sudoSetValue(Object newValue) {
            HardcodedProperty.this.sudoSetValue(newValue);
        }

        /**
         * Force get value
         *
         * @return new value to get
         */
        public Object sudoGetValue() {
            return HardcodedProperty.this.sudoGetValue();
        }

        /**
         * Force set element's value
         *
         * @param index    to find element
         * @param newValue to set
         */
        public void sudoSetValue(int index, Object newValue) {
            HardcodedProperty.this.sudoSetValue(index, newValue);
        }

        /**
         * Force get element's value
         *
         * @param index to find element
         * @return element's value
         */
        public Object sudoGetValue(int index) {
            return HardcodedProperty.this.sudoGetValue(index);
        }

        /**
         * Force set of new access level
         *
         * @param newAccessLevel to set
         */
        public void sudoSetAccessLevel(EnumSet<AccessManager.AccessRight> newAccessLevel) {
            HardcodedProperty.this.sudoSetAccessLevel(newAccessLevel);
        }
    }
}
