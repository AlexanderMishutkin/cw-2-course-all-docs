/*
 * @author apmishutkin@edu.hse.ru
 */

package properties;

import beans.Bean;
import introspection.PropertyHelper;
import type.Type;
import type.hardcoded.HardcodedType;

import java.beans.PropertyDescriptor;

/**
 * Class that get and stores object's properties info for beans or normal classes
 * <p>
 * Used in <code>BeanFunctionalProvider</code> to get <code>PropertyInfo</code> to implement property
 */
public class PropertiesInfoProvider {
    private final PropertyInfo[] propertyInfos;

    /**
     * @return array of <code>PropertyInfo</code> for given bean or normal java class
     */
    public PropertyInfo[] getPropertiesInfo() {
        return propertyInfos;
    }

    /**
     * Uses introspection to get info about class' properties. Use it for BeanVM beans
     * <p>
     * Pretty much the same as from class constructor,
     * but uses values of properties of prototype as default property values
     *
     * @param prototype bean prototype - default values container
     */
    public PropertiesInfoProvider(Bean prototype) {
        this(((HardcodedType) prototype.getType()).getImplementationClass());
    }

    /**
     * Uses introspection to get info about class' properties. Use this for common classes, not for beans
     *
     * @param targetClass class owner of properties
     */
    public PropertiesInfoProvider(Class<?> targetClass) {
        final PropertyDescriptor[] propertyDescriptors =
                PropertyHelper.getPropertyDescriptors(targetClass).toArray(new PropertyDescriptor[]{});
        propertyInfos = new PropertyInfo[propertyDescriptors.length];
        for (int i = 0; i < propertyInfos.length; i++) {
            final PropertyDescriptor pd = propertyDescriptors[i];
            propertyInfos[i] = new PropertyInfo(
                    pd.getName(),
                    Type.forClass(PropertyHelper.getPropertyValueClass(pd)),
                    PropertyHelper.getAccessLevel(pd)
            );
        }
    }
}
