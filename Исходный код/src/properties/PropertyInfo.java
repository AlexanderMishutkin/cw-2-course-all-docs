/*
 * @author apmishutkin@edu.hse.ru
 */

package properties;

import type.Type;

import java.util.EnumSet;


/**
 * Information about property: it's type, name and access level
 * <p>
 * Created from <code>PropertyDescriptor</code>
 */
public class PropertyInfo {
    private final String name;
    private final Type type;
    private final EnumSet<AccessManager.AccessRight> accessLevel;

    /**
     * @param name        of property
     * @param type        of property's value
     * @param accessLevel to this property
     */
    public PropertyInfo(String name, Type type, EnumSet<AccessManager.AccessRight> accessLevel) {
        this.name = name;
        this.type = type;
        this.accessLevel = accessLevel;
    }

    /**
     * @return type of property
     */
    public Type getType() {
        return type;
    }

    /**
     * @return access level to this property
     */
    public EnumSet<AccessManager.AccessRight> getAccessLevel() {
        return accessLevel.clone();
    }

    /**
     * @return name of property
     */
    public String getName() {
        return name;
    }
}
