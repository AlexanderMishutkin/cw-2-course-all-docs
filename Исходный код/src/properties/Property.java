/*
 * @author apmishutkin@edu.hse.ru
 */

package properties;

import events.NamedPropertyChangeListener;
import events.UpdatableNode;
import exceptions.BeanVM_AccessLevelException;
import exceptions.BeanVM_Exception;
import sensetive.memory.Variable;
import type.Type;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EnumSet;

/**
 * Abstract class defining main <code>Property</code> functionality
 */
abstract public class Property extends UpdatableNode implements PropertyChangeListener {

    /**
     * @param parent Node to delegate event handling to
     * @param value  variable used for implementing property
     * @param <T>    Node to delegate event handling to. Actually, <code>BeanFunctionalProvider</code> is expected
     */
    <T extends UpdatableNode & NamedPropertyChangeListener> Property(T parent, Variable value) {
        super(value, parent);
    }

    /**
     * @return level of access to property
     */
    abstract public EnumSet<AccessManager.AccessRight> getAccessLevel();

    /**
     * @param newAccessLevel should be lower (or equal) than current access level
     * @throws BeanVM_Exception if access level was tried to be widen
     */
    final public void setAccessLevel(EnumSet<AccessManager.AccessRight> newAccessLevel) {
        if (getAccessLevel().containsAll(newAccessLevel)) {
            sudoSetAccessLevel(newAccessLevel);
        } else {
            throw new BeanVM_Exception(
                    String.format("New access level (%s) should be smaller (or equal) than old (%s).\n" +
                                    "Or use 'sudoSetAccessLevel' version of method",
                            AccessManager.toString(newAccessLevel),
                            AccessManager.toString(getAccessLevel())));
        }
    }

    /**
     * @return is it array type property?
     */
    final public boolean isIndexed() {
        return getValueType().isArrayType();
    }

    /**
     * Force set of new access level
     *
     * @param newAccessLevel to set
     */
    abstract protected void sudoSetAccessLevel(EnumSet<AccessManager.AccessRight> newAccessLevel);

    /**
     * @return property name
     */
    abstract public String getName();

    /**
     * @return type of property's value
     */
    abstract public Type getValueType();

    /**
     * Property must be bindable!
     *
     * @param listener new listener to bind
     * @throws BeanVM_AccessLevelException if property is unbindable
     */
    final public void addPropertyChangeListener(PropertyChangeListener listener) {
        if (getAccessLevel().contains(AccessManager.AccessRight.BINDABLE)) {
            sudoAddPropertyChangeListener(listener);
        } else {
            throw new BeanVM_AccessLevelException(getName(), "add listener", AccessManager.bindable(),
                    getAccessLevel());
        }
    }

    /**
     * Redundant property-bindable check
     *
     * @param listener existing listener to unbind
     * @throws BeanVM_AccessLevelException if property is unbindable
     *                                     <p>
     *                                     TODO: Is this check necessary? What to do if listener was bound before property made unbindable?..
     */
    final public void removePropertyChangeListener(PropertyChangeListener listener) {
        if (getAccessLevel().contains(AccessManager.AccessRight.BINDABLE)) {
            sudoRemovePropertyChangeListener(listener);
        } else {
            throw new BeanVM_AccessLevelException(getName(), "remove listener", AccessManager.bindable(),
                    getAccessLevel());
        }
    }

    /**
     * Force bind listener
     *
     * @param listener to bind
     */
    abstract protected void sudoAddPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Force remove listener
     *
     * @param listener to unbind
     */
    abstract protected void sudoRemovePropertyChangeListener(PropertyChangeListener listener);

    /**
     * Checks for <code>WRITABLE</code> access right
     *
     * @param newValue to set
     * @throws BeanVM_AccessLevelException if property is immutable
     */
    final public void setValue(Object newValue) {
        if (getAccessLevel().contains(AccessManager.AccessRight.WRITABLE)) {
            sudoSetValue(newValue);
        } else {
            throw new BeanVM_AccessLevelException(getName(), "set value", AccessManager.writable(), getAccessLevel());
        }
    }

    /**
     * Checks for <code>READABLE</code> access right
     *
     * @return property value
     * @throws BeanVM_AccessLevelException if property isn't <code>READABLE</code>
     */
    final public Object getValue() {
        if (getAccessLevel().contains(AccessManager.AccessRight.READABLE)) {
            return sudoGetValue();
        } else {
            throw new BeanVM_AccessLevelException(getName(), "get value", AccessManager.readable(), getAccessLevel());
        }
    }

    /**
     * Checks for <code>INDEX_WRITABLE</code> access right
     *
     * @param elementIndex to find element to alter
     * @param newValue     to set
     * @throws BeanVM_AccessLevelException if property isn't <code>INDEX_WRITABLE</code>
     */
    final public void setValue(int elementIndex, Object newValue) {
        if (getAccessLevel().contains(AccessManager.AccessRight.INDEXED_WRITABLE)) {
            sudoSetValue(elementIndex, newValue);
        } else {
            throw new BeanVM_AccessLevelException(getName(), "set array element value", AccessManager.indexedWritable(),
                    getAccessLevel());
        }
    }

    /**
     * Checks for <code>INDEX_READABLE</code> access right
     *
     * @param elementIndex to find element
     * @return property's element value
     * @throws BeanVM_AccessLevelException if property isn't <code>INDEX_READABLE</code>
     */
    final public Object getValue(int elementIndex) {
        if (getAccessLevel().contains(AccessManager.AccessRight.INDEXED_WRITABLE)) {
            return sudoGetValue(elementIndex);
        } else {
            throw new BeanVM_AccessLevelException(getName(), "get array element value", AccessManager.indexedWritable(),
                    getAccessLevel());
        }
    }

    /**
     * Force set value
     *
     * @param newValue new value to set
     */
    abstract protected void sudoSetValue(Object newValue);

    /**
     * Force get value
     *
     * @return new value to get
     */
    abstract protected Object sudoGetValue();

    /**
     * Force set element's value
     *
     * @param index    to find element
     * @param newValue to set
     */
    abstract protected void sudoSetValue(int index, Object newValue);

    /**
     * Force get element's value
     *
     * @param index to find element
     * @return element's value
     */
    abstract protected Object sudoGetValue(int index);

    /**
     * This method gets called when a bound property is changed.
     *
     * @param evt A PropertyChangeEvent object describing the event source
     *            and the property that has changed.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        ((NamedPropertyChangeListener) getParent()).propertyChange(getName(), evt);
    }
}
