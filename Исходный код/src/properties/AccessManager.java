/*
 * @author apmishutkin@edu.hse.ru
 */

package properties;

import java.util.EnumSet;

/**
 * Utility class that helps to set up access level. Access level is <code>EnumSet</code> of <code>AccessRight</code>
 */
public class AccessManager {
    /**
     * Enum describing one possible type of <code>Bean</code> property access
     */
    public enum AccessRight {
        /**
         * Anyone can get value of property. Property can be immutable with such right
         */
        READABLE("r"),
        /**
         * Anyone can set value of property. Property can't be immutable with such right
         */
        WRITABLE("w"),
        /**
         * Anyone can subscribe to the <code>PropertyChangedEvent</code> sent by this property
         */
        BINDABLE("b"),
        /**
         * Anyone can get value of array property. Property can be immutable with such right
         */
        INDEXED_READABLE("R"),
        /**
         * Anyone can set value of index property. Property can't be immutable with such right
         */
        INDEXED_WRITABLE("W");

        public final String symbol;

        /**
         * @param symbol short one letter abbreviate of the <code>AccessRight</code>
         */
        AccessRight(String symbol) {
            this.symbol = symbol;
        }
    }

    /**
     * Anyone can get value of property. Property is immutable with this access level
     */
    public static EnumSet<AccessRight> readable() {
        return EnumSet.of(AccessRight.READABLE);
    }

    /**
     * Anyone can set value of property. Property isn't immutable with this access level
     */
    public static EnumSet<AccessRight> writable() {
        return EnumSet.of(AccessRight.WRITABLE);
    }

    /**
     * Anyone can subscribe to the <code>PropertyChangedEvent</code> sent by this property
     */
    public static EnumSet<AccessRight> bindable() {
        return EnumSet.of(AccessRight.BINDABLE);
    }

    /**
     * Anyone can get value of array property. Property is immutable with this access level
     */
    public static EnumSet<AccessRight> indexedReadable() {
        return EnumSet.of(AccessRight.INDEXED_READABLE);
    }

    /**
     * Anyone can set value of index property. Property isn't immutable with this access level
     */
    public static EnumSet<AccessRight> indexedWritable() {
        return EnumSet.of(AccessRight.INDEXED_WRITABLE);
    }

    /**
     * Full access access level for non array property
     */
    public static final EnumSet<AccessRight> ALL_SCALAR = EnumSet.range(AccessRight.READABLE, AccessRight.BINDABLE);
    /**
     * Full access access level for array property
     */
    public static final EnumSet<AccessRight> ALL_VECTOR = EnumSet.allOf(AccessRight.class);

    /**
     * Cast bitmask to <code>EnumSet</code> of <code>AccessRight</code>
     * <p>
     * For example:
     * <pre>
     *     bitmask: 0b10110
     *     bits:      12345
     *
     *     bit 1 id 1 = readable
     *     bit 2 is 0 = not writable
     *     bit 3 is 1 = bindable
     *     bit 4 is 1 = index readable (so it's array property)
     *     bit 5 is 0 = not index writable
     *
     *     So this mask is for bindable immutable array property
     * </pre>
     *
     * @param mask for instance <code>0b10110</code>
     * @return <code>EnumSet</code> of <code>AccessRight</code> coded by bitmask
     */
    public static EnumSet<AccessRight> fromBitmask(long mask) {
        EnumSet<AccessRight> result = EnumSet.noneOf(AccessRight.class);
        mask = mask & 0b11111;
        if ((mask & 0b00001) != 0) {
            result.add(AccessRight.INDEXED_WRITABLE);
        }
        if ((mask & 0b00010) != 0) {
            result.add(AccessRight.INDEXED_READABLE);
        }
        if ((mask & 0b00100) != 0) {
            result.add(AccessRight.BINDABLE);
        }
        if ((mask & 0b01000) != 0) {
            result.add(AccessRight.WRITABLE);
        }
        if ((mask & 0b10000) != 0) {
            result.add(AccessRight.READABLE);
        }
        return result;
    }

    /**
     * Just set flags to boolean values to get access level
     *
     * @return <code>EnumSet</code> of <code>AccessRight</code> as it is set up by the flag
     */
    public static EnumSet<AccessRight> fromFlags(boolean isReadable, boolean isWritable, boolean isBindable,
                                                 boolean isIndexReadable, boolean isIndexWritable) {
        EnumSet<AccessRight> result = EnumSet.noneOf(AccessRight.class);
        if (isReadable) {
            result.add(AccessRight.READABLE);
        }
        if (isWritable) {
            result.add(AccessRight.WRITABLE);
        }
        if (isBindable) {
            result.add(AccessRight.BINDABLE);
        }
        if (isIndexReadable) {
            result.add(AccessRight.INDEXED_READABLE);
        }
        if (isIndexWritable) {
            result.add(AccessRight.INDEXED_WRITABLE);
        }
        return result;
    }

    /**
     * Cast string to <code>EnumSet</code> of <code>AccessRight</code>
     * <p>
     * For example:
     * <pre>
     *      string: "r-bR-"
     *
     *      r = readable
     *      - = not writable
     *      b = bindable
     *      R = index readable (so it's array property)
     *      - = not index writable
     * </pre>
     *
     * @param flags for instance <code>"r-bR-"</code>
     * @return <code>EnumSet</code> of <code>AccessRight</code> coded by string
     */
    public static EnumSet<AccessRight> fromString(String flags) {
        EnumSet<AccessRight> result = EnumSet.noneOf(AccessRight.class);
        for (int i = 0; i < flags.length(); i++) {
            switch (flags.charAt(i)) {
                case 'r':
                    result.add(AccessRight.READABLE);
                    continue;
                case 'w':
                    result.add(AccessRight.WRITABLE);
                    continue;
                case 'b':
                    result.add(AccessRight.BINDABLE);
                    continue;
                case 'R':
                    result.add(AccessRight.INDEXED_READABLE);
                    continue;
                case 'W':
                    result.add(AccessRight.INDEXED_WRITABLE);
            }
        }
        return result;
    }

    /**
     * Cast <code>EnumSet</code> of <code>AccessRight</code> to string
     * <p>
     * For example:
     * <pre>
     *      string: "r-bR-"
     *
     *      r = readable
     *      - = not writable
     *      b = bindable
     *      R = index readable (so it's array property)
     *      - = not index writable
     * </pre>
     *
     * @param accessLevel <code>EnumSet</code> of <code>AccessRight</code>
     * @return string representation of <code>EnumSet</code> of <code>AccessRight</code>
     */
    public static String toString(EnumSet<AccessRight> accessLevel) {
        String answer = "";
        if (accessLevel.containsAll(readable())) {
            answer += "r";
        } else {
            answer += "-";
        }
        if (accessLevel.containsAll(writable())) {
            answer += "w";
        } else {
            answer += "-";
        }
        if (accessLevel.containsAll(bindable())) {
            answer += "b";
        } else {
            answer += "-";
        }
        if (accessLevel.containsAll(indexedReadable())) {
            answer += "R";
        } else {
            answer += "-";
        }
        if (accessLevel.containsAll(indexedWritable())) {
            answer += "W";
        } else {
            answer += "-";
        }
        return answer;
    }

    /**
     * Cast to bitmask <code>EnumSet</code> of <code>AccessRight</code>
     * <p>
     * For example:
     * <pre>
     *     bitmask: 0b10110
     *     bits:      12345
     *
     *     bit 1 id 1 = readable
     *     bit 2 is 0 = not writable
     *     bit 3 is 1 = bindable
     *     bit 4 is 1 = index readable (so it's array property)
     *     bit 5 is 0 = not index writable
     *
     *     So this mask is for bindable immutable array property
     * </pre>
     *
     * @param accessLevel <code>EnumSet</code> of <code>AccessRight</code>
     * @return <code>EnumSet</code> of <code>AccessRight</code> coded by bitmask
     */
    public static long toBitmask(EnumSet<AccessRight> accessLevel) {
        long result = 0;
        if (accessLevel.contains(AccessRight.INDEXED_WRITABLE)) {
            result = result | 0b0001;
        }
        if (accessLevel.contains(AccessRight.INDEXED_READABLE)) {
            result = result | 0b00010;
        }
        if (accessLevel.contains(AccessRight.BINDABLE)) {
            result = result | 0b00100;
        }
        if (accessLevel.contains(AccessRight.WRITABLE)) {
            result = result | 0b01000;
        }
        if (accessLevel.contains(AccessRight.READABLE)) {
            result = result | 0b10000;
        }
        return result;
    }
}
