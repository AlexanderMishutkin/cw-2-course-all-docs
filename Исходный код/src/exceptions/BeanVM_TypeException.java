/*
 * @author apmishutkin@edu.hse.ru
 */

package exceptions;

import org.jetbrains.annotations.NotNull;
import type.Type;

/**
 * Exception thrown when some type do not fit BeanVM expectation
 */
public class BeanVM_TypeException extends BeanVM_Exception {
    /**
     * @param where    exception happens
     * @param expected type
     * @param actual   type
     */
    public BeanVM_TypeException(@NotNull String where, @NotNull Type expected, Type actual) {
        super(String.format("In %s expected Type \"%s\" got \"%s\"", where, expected.getName(),
                actual == null ? "NULL" : actual.getName()));
    }
}
