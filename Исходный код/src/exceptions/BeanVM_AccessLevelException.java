/*
 * @author apmishutkin@edu.hse.ru
 */

package exceptions;

import org.jetbrains.annotations.NotNull;
import properties.AccessManager;

import java.util.EnumSet;

/**
 * Wrong access level access exception
 * <p>
 * Raised by setters and getters of <code>Property</code> for instance
 */
public class BeanVM_AccessLevelException extends BeanVM_Exception {
    /**
     * @param subject             where exception occurred
     * @param operation           what user was trying to do
     * @param requiredAccessLevel minimal access level for successful operation (usually 1 right)
     * @param actualAccessLevel   of <code>Property</code> or other accessed object
     */
    public BeanVM_AccessLevelException(@NotNull String subject, @NotNull String operation,
                                       @NotNull EnumSet<AccessManager.AccessRight> requiredAccessLevel,
                                       @NotNull EnumSet<AccessManager.AccessRight> actualAccessLevel) {
        super(String.format("AccessLevel error occurred while trying to do operation \"%s\" on %s \n" +
                        "%s\t%s // Actual access level \n" +
                        "%s\t%s // Was required by operation",
                operation,
                subject,
                AccessManager.toString(actualAccessLevel),
                subject,
                AccessManager.toString(AccessManager.fromBitmask(AccessManager.toBitmask(requiredAccessLevel)
                        | AccessManager.toBitmask(actualAccessLevel))),
                subject));
    }
}
