/*
 * @author apmishutkin@edu.hse.ru
 */

package data.structures;

public interface ArrayWrapper extends Wrapper {
    /**
     * @param index index in wrapped array
     * @return data wrapped by this wrapper under given index
     */
    Object getValue(int index);
}
