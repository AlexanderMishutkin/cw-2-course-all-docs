/*
 * @author apmishutkin@edu.hse.ru
 */

package data.structures;

/**
 * Used to wrap any data with some logic
 */
public interface Wrapper {
    /**
     * @return data wrapped by this wrapper
     */
    Object getValue();
}
