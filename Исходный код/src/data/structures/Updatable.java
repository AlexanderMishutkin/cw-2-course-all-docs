/*
 * @author apmishutkin@edu.hse.ru
 */

package data.structures;

/**
 * Interface to track content changes caused by events in <code>EventCascade</code>
 * <p>
 * Each context object implementing this interface can be set changed and then process changes when update is called
 */
public interface Updatable {
    /**
     * Accept changes
     */
    void change();

    /**
     * Process accepted changes
     */
    void update();

    /**
     * Is element changed and never updated after it?
     */
    boolean isChanged();
}
