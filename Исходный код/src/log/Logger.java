/*
 * @author apmishutkin@edu.hse.ru
 */

package log;

/**
 * Turn Bean VM object to String using different formatters
 */
public class Logger {
    /**
     * Class that conduct parsing <code>Loggable</code> object to string
     */
    static public abstract class Formatter {
        /**
         * @param object to format
         * @return string representation of object
         */
        public abstract String toStringForm(Loggable object);
    }

    private static Formatter defaultFormatter = new DefaultFormatter();

    /**
     * @return default formatter
     */
    public static Formatter getDefaultFormatter() {
        return defaultFormatter;
    }

    /**
     * @param defaultFormatter new formatter to use by default
     */
    public static void setDefaultFormatter(Formatter defaultFormatter) {
        Logger.defaultFormatter = defaultFormatter;
    }


    /**
     * Use default formatter to stringify given object
     *
     * @param object to stringify
     * @return object formatted by default formatter
     */
    public static String toStringForm(Loggable object) {
        return defaultFormatter.toStringForm(object);
    }

    /**
     * Use given formatter to stringify given object
     *
     * @param object    to stringify
     * @param formatter new formatter to use in this case
     * @return object formatted by given formatter
     */
    public static String toStringForm(Loggable object, Formatter formatter) {
        return formatter.toStringForm(object);
    }
}
