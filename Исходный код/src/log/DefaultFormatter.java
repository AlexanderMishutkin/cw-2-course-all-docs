/*
 * @author apmishutkin@edu.hse.ru
 */

package log;

import beans.Bean;
import introspection.ArrayHelper;
import properties.AccessManager;
import properties.PropertiesInfoProvider;
import properties.PropertyInfo;
import type.Type;
import type.hardcoded.BeanVM_BeanType;
import type.hardcoded.HardcodedType;
import type.hardcoded.JavaArrayType;

import java.util.ArrayList;
import java.util.List;

/**
 * Default formatter implementation
 */
public class DefaultFormatter extends Logger.Formatter {
    /**
     * @param type to format
     * @return representation of object as number of strings
     */
    ArrayList<String> typeToStringList(Type type) {
        if (type.isArrayType()) {
            ArrayList<String> element = typeToStringList(((JavaArrayType) type).getElementType());
            element.set(0, element.get(0) + "[]");
            return element;
        }
        if (type.isBeanType()) {
            BeanVM_BeanType beanType = (BeanVM_BeanType) type;
            PropertiesInfoProvider properties = beanType.getPropertiesInfoProvider();
            ArrayList<String> formatted = new ArrayList<>();
            formatted.add(beanType.getName() + ":");
            PropertyInfo[] propertiesInfo = properties.getPropertiesInfo();
            for (int i = 0, propertiesInfoLength = propertiesInfo.length; i < propertiesInfoLength; i++) {
                PropertyInfo property = propertiesInfo[i];
                if (i == propertiesInfoLength - 1) {
                    formatted.add(String.format("\t%s\t%s",
                            AccessManager.toString(property.getAccessLevel()), property.getName()));
                } else {
                    formatted.add(String.format("\t%s\t%s,",
                            AccessManager.toString(property.getAccessLevel()), property.getName()));
                }
            }
            return formatted;
        }
        return new ArrayList<>(List.of(type.getName()));
    }

    /**
     * @param bean to format
     * @return representation of object as number of strings
     */
    ArrayList<String> beanToStringList(Bean bean) {
        Type type = bean.getType();
        ArrayList<String> formatted = new ArrayList<>();
        formatted.add(type.getName() + ":");

        PropertyInfo[] properties = ((HardcodedType) type).getPropertiesInfoProvider().getPropertiesInfo();
        for (int i = 0, n = properties.length; i < n; i++) {
            PropertyInfo property = properties[i];

            if (bean.getPropertyValue(property.getName()) != null) {
                if (property.getType().isBeanArrayType()) {
                    formatted.add(String.format("\t%s\t%s = [",
                            AccessManager.toString(property.getAccessLevel()), property.getName()));

                    Object[] beans = (Object[]) bean.getPropertyValue(property.getName());
                    for (int j = 0; j < beans.length; j++) {
                        ArrayList<String> innerBean = beanToStringList((Bean) beans[j]);
                        innerBean.replaceAll((s) -> "\t\t" + s);
                        formatted.addAll(innerBean);

                        if (j != beans.length - 1) {
                            formatted.set(formatted.size() - 1, formatted.get(formatted.size() - 1) + ",");
                        }
                    }

                    if (i != n - 1) {
                        formatted.set(formatted.size() - 1, formatted.get(formatted.size() - 1) + " ],");
                    } else {
                        formatted.set(formatted.size() - 1, formatted.get(formatted.size() - 1) + " ]");
                    }
                    continue;
                }

                if (property.getType().isBeanType()) {
                    formatted.add(String.format("\t%s\t%s =",
                            AccessManager.toString(property.getAccessLevel()), property.getName()));

                    ArrayList<String> innerBean = beanToStringList((Bean) bean.getPropertyValue(property.getName()));
                    innerBean.replaceAll((s) -> "\t\t" + s);
                    formatted.addAll(innerBean);

                    if (i != n - 1) {
                        formatted.set(i + 1, formatted.get(i + 1) + ",");
                    }
                    continue;
                }

            }

            formatted.add(String.format("\t%s\t%s = %s,",
                    AccessManager.toString(property.getAccessLevel()), property.getName(),
                    ArrayHelper.arrayObjectToString(bean.getPropertyValue(property.getName()))));

            if (i == n - 1) {
                formatted.set(i + 1, formatted.get(i + 1).replaceAll("[,]$", ""));
            }
        }
        return formatted;
    }

    /**
     * @param object to format
     * @return representation of object as number of strings
     */
    ArrayList<String> toStringList(Loggable object) {
        if (object instanceof Bean) {
            return beanToStringList((Bean) object);
        }
        if (object instanceof Type) {
            return typeToStringList((Type) object);
        }

        throw new UnsupportedOperationException("This class is not expected to be formatted as string by default " +
                "formatter!\nHowever, you can override formatter to output your class");
    }

    /**
     * @param object to format
     * @return string representation of object
     */
    @Override
    public String toStringForm(Loggable object) {
        return String.join("\n", toStringList(object));
    }
}
