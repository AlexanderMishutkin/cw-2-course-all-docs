/*
 * @author apmishutkin@edu.hse.ru
 */

package log;

/**
 * Class that can be formatted as string
 * <p>
 * Author guess there is no need to instantiate this class itself
 */
abstract public class Loggable {
    @Override
    public String toString() {
        return toStringForm();
    }

    /**
     * Returns formal and quite full representation of Bean VM object as string
     * <p>
     * The format can be set up in via <code>Logger.setDefaultFormatter()</code>
     *
     * @return formal and quite full representation of Bean VM object as string
     */
    public String toStringForm() {
        return Logger.toStringForm(this);
    }
}
