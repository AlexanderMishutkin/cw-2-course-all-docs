/*
 * @author apmishutkin@edu.hse.ru
 */

package introspection;

import java.beans.*;

/**
 * Helps in work with JavaBeans API
 */
public class BeanHelper {
    private static final BeanInfo OBJECT_BEAN_INFO = new SimpleBeanInfo() {
        public BeanDescriptor getBeanDescriptor() {
            return new BeanDescriptor(Object.class);
        }

        public PropertyDescriptor[] getPropertyDescriptors() {
            return new PropertyDescriptor[]{};
        }
    };

    private static final BeanInfo ENUM_BEAN_INFO = new SimpleBeanInfo() {
        public BeanDescriptor getBeanDescriptor() {
            return new BeanDescriptor(Enum.class);
        }

        public PropertyDescriptor[] getPropertyDescriptors() {
            return new PropertyDescriptor[]{};
        }
    };

    /**
     * We use Object.class as a stopClass in Introspection to hide Class propertyType (and avoid its' propertyTypes
     * classes registering). More precisely, that way we always hide "class" property, that any class contains.
     * But in case a method that returns Class or gets Class as an property accessor type
     * we cannot avoid Class related staff registering (see, e.g,  Type.getTypePrototypeForClass(PropertyDescriptor.class))
     * TODO: work around needed:
     * 1. We have to decide - do we need to hide Class and its property types from registering?...
     * 2. In case - yes, we can do it while registering (at the latest stage).
     * 3. But - it seems - we have to say - NO (it might be useful).
     * 4. Any way - we have to deal with arrays separately (and register their elementType(s) only...)
     *
     * @param c class to extract BeanInfo from
     * @return beanInfo
     */
    public static BeanInfo getBeanInfo(Class<?> c) {
        BeanInfo result = OBJECT_BEAN_INFO;
        try {
            if (c.isEnum()) { // that is a hack to hide properties from enums...
                return ENUM_BEAN_INFO;
            }
            if (c.isPrimitive() || c.isInterface()) {
                result = Introspector.getBeanInfo(c);
            } else if (Object.class != c) {
                result = Introspector.getBeanInfo(c, Object.class);  //to hide getClass() method...
            }
        } catch (Exception ex) {
            System.err.println("BeanHelper.getBeanInfo(); Exception: " + ex);
            ex.printStackTrace();
        }
        return result;
    }
}
