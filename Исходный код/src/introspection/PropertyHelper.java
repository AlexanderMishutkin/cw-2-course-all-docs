/*
 * @author apmishutkin@edu.hse.ru
 */

package introspection;

import properties.AccessManager;

import java.beans.IndexedPropertyDescriptor;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Helps to work with JavaBean API property descriptors
 */
public class PropertyHelper {
    /**
     * Java Beans property descriptors are extracted using introspection, but in that case we'll get too much
     * properties that are not relevant to our goals. That's why we use the implementation of our own, that
     * drops out extra properties from Class.class, etc.
     * <p>
     * In case the class is instantiable class, property descriptors are assigned with default values...
     *
     * @param c - class to get property descriptors from;
     * @return - resulting property descriptors.
     */
    public static List<PropertyDescriptor> getPropertyDescriptors(Class<?> c) {
        List<PropertyDescriptor> descriptorList = new ArrayList<>();
        Class<?> superclass = c.getSuperclass();

        if (superclass != null) {
            descriptorList = getPropertyDescriptors(superclass);
        }

        PropertyDescriptor[] propertyDescriptors = new PropertyDescriptor[]{};
        try {
            propertyDescriptors = BeanHelper.getBeanInfo(c).getPropertyDescriptors();
        } catch (Exception ex) {
            System.err.println("BeanHelper.getPropertyDescriptors(): Exception: " + ex);
        }

        for (PropertyDescriptor pd : propertyDescriptors) {
            String name = pd.getName();
            List<String> descriptorNames = getNamesFromPropertyDescriptors(descriptorList);
            if (!descriptorNames.contains(name) && !name.equals("type")) {
                descriptorList.add(pd);
            }
        }
        return descriptorList;
    }

    /**
     * @return a list of property names
     */
    private static List<String> getNamesFromPropertyDescriptors(List<PropertyDescriptor> descriptors) {
        List<String> result = new ArrayList<>(descriptors.size());
        result.addAll(descriptors.stream().map(PropertyDescriptor::getName).collect(Collectors.toList()));
        return result;
    }

    /**
     * @return class of property value
     */
    public static Class<?> getPropertyValueClass(PropertyDescriptor propertyDescriptor) {
        Class<?> result = propertyDescriptor.getPropertyType();
        if (result == null) {
            if (propertyDescriptor instanceof IndexedPropertyDescriptor) {
                Class<?> indexedValueClass = ((IndexedPropertyDescriptor) propertyDescriptor).getIndexedPropertyType();
                result = java.lang.reflect.Array.newInstance(indexedValueClass, 0).getClass();
            }
        }
        return result;
    }

    /**
     * @return <code>EnumSet AccessManager.AccessRight </code> that characterise access level to the property
     */
    public static EnumSet<AccessManager.AccessRight> getAccessLevel(PropertyDescriptor propertyDescriptor) {
        return AccessManager.fromFlags(
                PropertyHelper.isReadable(propertyDescriptor),
                PropertyHelper.isWritable(propertyDescriptor),
                PropertyHelper.isBindable(propertyDescriptor),
                PropertyHelper.isIndexedReadable(propertyDescriptor),
                PropertyHelper.isIndexedWritable(propertyDescriptor));
    }

    private static boolean isReadable(PropertyDescriptor propertyDescriptor) {
        return propertyDescriptor.getReadMethod() != null;
    }

    private static boolean isWritable(PropertyDescriptor propertyDescriptor) {
        return propertyDescriptor.getWriteMethod() != null;
    }

    private static boolean isBindable(PropertyDescriptor propertyDescriptor) {
        return propertyDescriptor.isBound();
    }

    private static boolean isIndexedReadable(PropertyDescriptor propertyDescriptor) {
        return propertyDescriptor instanceof IndexedPropertyDescriptor &&
                ((IndexedPropertyDescriptor) propertyDescriptor).getIndexedReadMethod() != null;
    }

    private static boolean isIndexedWritable(PropertyDescriptor propertyDescriptor) {
        return propertyDescriptor instanceof IndexedPropertyDescriptor &&
                ((IndexedPropertyDescriptor) propertyDescriptor).getIndexedWriteMethod() != null;
    }
}
