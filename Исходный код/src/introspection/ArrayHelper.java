/*
 * @author apmishutkin@edu.hse.ru
 */

package introspection;

import java.util.Arrays;

/**
 * Helps to use reflection to work with array as objects
 */
public class ArrayHelper {
    /**
     * The method clones its input value in case of it is non-empty array
     *
     * @param value - input object
     * @return - the input object value itself or its clone if it is non-empty array.
     */
    public static Object cloneValueIfArray(final Object value) {
        Object result = value;
        if (value != null && value.getClass().isArray()) {
            if (value instanceof boolean[]) {
                boolean[] booleans = (boolean[]) value;
                if (booleans.length > 0) {
                    result = booleans.clone();
                }
            } else if (value instanceof byte[]) {
                byte[] bytes = (byte[]) value;
                if (bytes.length > 0) {
                    result = bytes.clone();
                }
            } else if (value instanceof char[]) {
                char[] chars = (char[]) value;
                if (chars.length > 0) {
                    result = chars.clone();
                }
            } else if (value instanceof short[]) {
                short[] shorts = (short[]) value;
                if (shorts.length > 0) {
                    result = shorts.clone();
                }
            } else if (value instanceof int[]) {
                int[] ints = (int[]) value;
                if (ints.length > 0) {
                    result = ints.clone();
                }
            } else if (value instanceof float[]) {
                float[] floats = (float[]) value;
                if (floats.length > 0) {
                    result = floats.clone();
                }
            } else if (value instanceof double[]) {
                double[] doubles = (double[]) value;
                if (doubles.length > 0) {
                    result = doubles.clone();
                }
            } else if (value instanceof long[]) {
                long[] longs = (long[]) value;
                if (longs.length > 0) {
                    result = longs.clone();
                }
            } else if (value instanceof Object[]) {
                Object[] objects = (Object[]) value;
                if (objects.length > 0) {
                    result = objects.clone();
                }
            }
        }
        return result;
    }

    /**
     * Stringify array
     */
    public static String arrayObjectToString(Object arrayValue) {
        if (arrayValue == null) {
            return "NULL";
        }
        String result = arrayValue.toString();
        if (arrayValue instanceof boolean[]) {
            result = Arrays.toString((boolean[]) arrayValue);
        } else if (arrayValue instanceof byte[]) {
            result = Arrays.toString((byte[]) arrayValue);
        } else if (arrayValue instanceof char[]) {
            result = Arrays.toString((char[]) arrayValue);
        } else if (arrayValue instanceof short[]) {
            result = Arrays.toString((short[]) arrayValue);
        } else if (arrayValue instanceof int[]) {
            result = Arrays.toString((int[]) arrayValue);
        } else if (arrayValue instanceof float[]) {
            result = Arrays.toString((float[]) arrayValue);
        } else if (arrayValue instanceof long[]) {
            result = Arrays.toString((long[]) arrayValue);
        } else if (arrayValue instanceof double[]) {
            result = Arrays.toString((double[]) arrayValue);
        } else if (arrayValue instanceof Object[]) {
            result = Arrays.toString((Object[]) arrayValue);
        }
        return result;
    }

    /**
     * Returns value at given index
     */
    public static Object getArrayValueElement(Object arrayValue, int elementIndex) {
        Object result;
        if (arrayValue instanceof boolean[]) {
            result = ((boolean[]) arrayValue)[elementIndex];
        } else if (arrayValue instanceof byte[]) {
            result = ((byte[]) arrayValue)[elementIndex];
        } else if (arrayValue instanceof char[]) {
            result = ((char[]) arrayValue)[elementIndex];
        } else if (arrayValue instanceof short[]) {
            result = ((short[]) arrayValue)[elementIndex];
        } else if (arrayValue instanceof int[]) {
            result = ((int[]) arrayValue)[elementIndex];
        } else if (arrayValue instanceof float[]) {
            result = ((float[]) arrayValue)[elementIndex];
        } else if (arrayValue instanceof long[]) {
            result = ((long[]) arrayValue)[elementIndex];
        } else if (arrayValue instanceof double[]) {
            result = ((double[]) arrayValue)[elementIndex];
        } else {
            result = ((Object[]) arrayValue)[elementIndex];
        }
        return result;
    }

    /**
     * Set value of array element under given index
     */
    public static void setArrayValueElement(Object arrayValue, int elementIndex, Object newValue) {
        if (arrayValue instanceof boolean[]) {
            ((boolean[]) arrayValue)[elementIndex] = (Boolean) newValue;
        } else if (arrayValue instanceof byte[]) {
            ((byte[]) arrayValue)[elementIndex] = (Byte) newValue;
        } else if (arrayValue instanceof char[]) {
            ((char[]) arrayValue)[elementIndex] = (Character) newValue;
        } else if (arrayValue instanceof short[]) {
            ((short[]) arrayValue)[elementIndex] = (Short) newValue;
        } else if (arrayValue instanceof int[]) {
            ((int[]) arrayValue)[elementIndex] = (Integer) newValue;
        } else if (arrayValue instanceof float[]) {
            ((float[]) arrayValue)[elementIndex] = (Float) newValue;
        } else if (arrayValue instanceof long[]) {
            ((long[]) arrayValue)[elementIndex] = (Long) newValue;
        } else if (arrayValue instanceof double[]) {
            ((double[]) arrayValue)[elementIndex] = (Double) newValue;
        } else {
            ((Object[]) arrayValue)[elementIndex] = newValue;
        }
    }

    /**
     * Compares two arrays element by element
     */
    public static boolean areArraysEquals(Object arr1, Object arr2) {
        if (arr1 == null && arr2 == null) {
            return true;
        }
        if (arr1 == null) {
            return false;
        }
        if (arr1.equals(arr2)) {
            return true;
        }
        boolean res = false;
        try {
            if (arr1 instanceof boolean[]) {
                res = Arrays.equals((boolean[]) arr1, (boolean[]) arr2);
            } else if (arr1 instanceof byte[]) {
                res = Arrays.equals((byte[]) arr1, (byte[]) arr2);
            } else if (arr1 instanceof char[]) {
                res = Arrays.equals((char[]) arr1, (char[]) arr2);
            } else if (arr1 instanceof short[]) {
                res = Arrays.equals((short[]) arr1, (short[]) arr2);
            } else if (arr1 instanceof int[]) {
                res = Arrays.equals((int[]) arr1, (int[]) arr2);
            } else if (arr1 instanceof float[]) {
                res = Arrays.equals((float[]) arr1, (float[]) arr2);
            } else if (arr1 instanceof double[]) {
                res = Arrays.equals((double[]) arr1, (double[]) arr2);
            } else if (arr1 instanceof long[]) {
                res = Arrays.equals((long[]) arr1, (long[]) arr2);
            } else if (arr1 instanceof Object[]) {
                res = Arrays.equals((Object[]) arr1, (Object[]) arr2);
            }
        } catch (Exception ignored) {
        }
        return res;
    }
}
