/*
 * @author apmishutkin@edu.hse.ru
 */

package introspection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

/**
 * Helps to use reflection while working with objects
 */
public class ObjectHelper {
    /**
     * Compares two values (as arrays, if it is necessary)
     */
    public static boolean isEqual(Object o1, Object o2) {
        return o1 == null && o2 == null || ((o1 != null && ArrayHelper.areArraysEquals(o1, o2)) ||
                (o2 != null && ArrayHelper.areArraysEquals(o2, o1)));
    }

    /**
     * Does this class has public default constructor to be instantiated?
     */
    public static boolean isInstantiable(Class<?> c) {
        return isPublicNonAbstractClass(c) && hasDefaultConstructor(c);
    }

    private static boolean hasDefaultConstructor(Class<?> c) {
        try {
            Constructor<?> defaultConstructor = c.getConstructor();
            return Modifier.isPublic(defaultConstructor.getModifiers());
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean isPublicNonAbstractClass(Class<?> c) {
        int m = c.getModifiers();
        return Modifier.isPublic(m) && (!Modifier.isAbstract(m));
    }

    /**
     * Can we assign <code>value</code> to variable of type <code>valueClass</code>?
     */
    public static boolean isValueAssignable(Object value, Class<?> valueClass) {
        boolean result;
        if (valueClass.isPrimitive()) {
            if (valueClass == boolean.class) {
                result = (value instanceof Boolean);
            } else if (valueClass == float.class) {
                result = (value instanceof Float);
            } else if (valueClass == double.class) {
                result = (value instanceof Double);
            } else if (valueClass == int.class) {
                result = (value instanceof Integer);
            } else if (valueClass == byte.class) {
                result = (value instanceof Byte);
            } else if (valueClass == long.class) {
                result = (value instanceof Long);
            } else if (valueClass == short.class) {
                result = (value instanceof Short);
            } else if (valueClass == char.class) {
                result = (value instanceof Character);
            } else {
                throw new RuntimeException("isValueAssignable() for class " + valueClass + " NOT IMPLEMENTED.");
            }
        } else {
            result = valueClass.isInstance(value);
        }
        return result;
    }
}
