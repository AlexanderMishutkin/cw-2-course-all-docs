/*
 * @author apmishutkin@edu.hse.ru
 */

package sensetive.memory;

import data.structures.ArrayWrapper;
import events.EventCascade;
import introspection.ArrayHelper;
import introspection.ObjectHelper;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Object wrapper. Tracks changes and reports them to listeners.
 */
public class Variable implements ArrayWrapper {
    private Object value;
    private List<PropertyChangeListener> listeners = null;

    public Variable(Object initValue) {
        value = initValue;
    }

    /**
     * @param listener to add
     */
    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<>();
        }
        listeners.add(listener);
    }

    /**
     * @param listener to remove
     */
    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        listeners.remove(listener);
        if (listeners.isEmpty()) {
            listeners = null;
        }
    }

    /**
     * @return data wrapped by this wrapper
     */
    @Override
    public synchronized Object getValue() {
        return ArrayHelper.cloneValueIfArray(value);
    }

    /**
     * @param index if variable wraps array
     */
    @Override
    public synchronized Object getValue(int index) {
        return ArrayHelper.getArrayValueElement(value, index);
    }

    /**
     * @param newValue to set
     */
    public synchronized void setValue(Object newValue) {
        Object oldValue = value;
        value = ArrayHelper.cloneValueIfArray(newValue);
        valueChanged(oldValue, newValue);
    }

    /**
     * @param index if variable wraps array
     */
    public synchronized void setValue(int index, Object newValue) {
        Object oldValue = ArrayHelper.getArrayValueElement(value, index); //value[index];
        ArrayHelper.setArrayValueElement(value, index, newValue); //value[index] = newValue;
        indexedValueChanged(index, oldValue, newValue);
    }

    /**
     * Reports value change to each listener.
     * Uses <code>EventCascade</code> to control event delivery and context updating
     */
    private void valueChanged(Object oldValue, Object newValue) {
        if (listeners != null && !listeners.isEmpty()) {
            if (!ObjectHelper.isEqual(oldValue, newValue)) {
                PropertyChangeEvent event = EventCascade.createPropertyChangeEvent(this, "value",
                        oldValue, newValue);
                for (PropertyChangeListener listener : listeners) {
                    listener.propertyChange(event);
                }
                EventCascade.eventDelivered(event);
            }
        }
    }

    /**
     * Reports value change to each listener.
     * Uses <code>EventCascade</code> to control event delivery and context updating
     */
    private void indexedValueChanged(int index, Object oldValue, Object newValue) {
        if (listeners != null && !listeners.isEmpty()) {
            if (oldValue == null || !oldValue.equals(newValue)) {
                IndexedPropertyChangeEvent event = EventCascade.createIndexedPropertyChangeEvent(
                        this, "value", oldValue, newValue, index
                );
                for (PropertyChangeListener listener : listeners) {
                    listener.propertyChange(event);
                }
                EventCascade.eventDelivered(event);
            }
        }
    }
}
