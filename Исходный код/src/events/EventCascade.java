/*
 * @author apmishutkin@edu.hse.ru
 */

package events;

import sensetive.memory.Variable;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Helps to avoid event handling loops by splitting event handling on delivering and updating and
 * controlling both processes
 */
public class EventCascade {
    private EventCascade() {
    }

    static private int inDeliveryCnt = 0;
    static private Queue<UpdatableNode> contextRootsToUpdate = new LinkedList<>();

    public static void clearCache() {
        inDeliveryCnt = 0;
        contextRootsToUpdate = new LinkedList<>();
    }

    /**
     * @param updatableNode to update after all events will be delivered
     */
    synchronized public static void registerRootNodeToUpdate(UpdatableNode updatableNode) {
        contextRootsToUpdate.add(updatableNode);
    }

    /**
     * Creates copy of event but with altered source and property name
     *
     * @param evt          to recreate
     * @param source       variable that was changed
     * @param propertyName property that was changed
     * @return copy of event but with altered source and property name
     */
    synchronized public static PropertyChangeEvent recreatePropertyChangeEventFromExisting(PropertyChangeEvent evt,
                                                                                           Variable source,
                                                                                           String propertyName) {
        if (evt instanceof IndexedPropertyChangeEvent) {
            return createIndexedPropertyChangeEvent(source, propertyName, evt.getOldValue(), evt.getNewValue(),
                    ((IndexedPropertyChangeEvent) evt).getIndex());
        } else {
            return createPropertyChangeEvent(source, propertyName, evt.getOldValue(), evt.getNewValue());
        }
    }

    /**
     * Creates new event to deliver and track
     *
     * @param variable     that was changed
     * @param propertyName that was changed
     * @param oldValue     before it was changed
     * @param newValue     after variable was changed
     * @return event describing changes
     */
    synchronized public static PropertyChangeEvent createPropertyChangeEvent(Variable variable, String propertyName,
                                                                             Object oldValue,
                                                                             Object newValue) {
        inDeliveryCnt++;
        return new PropertyChangeEvent(variable, propertyName, oldValue, newValue);
    }

    /**
     * Call after all listeners are invoked
     *
     * @param event that was delivered
     */
    synchronized public static void eventDelivered(PropertyChangeEvent event) {
        inDeliveryCnt--;
        if (inDeliveryCnt == 0) {
            finishCascade();
        }
    }

    /*** Creates new event to deliver and track
     *
     * @param variable that was changed
     * @param propertyName that was changed
     * @param oldValue before it was changed
     * @param newValue after variable was changed
     * @param index of element in property
     * @return event describing changes
     */
    synchronized public static IndexedPropertyChangeEvent createIndexedPropertyChangeEvent(Variable variable,
                                                                                           String propertyName,
                                                                                           Object oldValue,
                                                                                           Object newValue,
                                                                                           int index) {
        inDeliveryCnt++;
        return new IndexedPropertyChangeEvent(variable, propertyName, oldValue, newValue, index);
    }

    /**
     * Called when all events are delivered to update nodes
     */
    synchronized private static void finishCascade() {
        while (!contextRootsToUpdate.isEmpty()) {
            UpdatableNode top = contextRootsToUpdate.poll();
            if (top.isChanged()) {
                top.update();
            }
        }
    }
}
