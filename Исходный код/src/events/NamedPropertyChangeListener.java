/*
 * @author apmishutkin@edu.hse.ru
 */

package events;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public interface NamedPropertyChangeListener extends PropertyChangeListener {
    void propertyChange(String propertyName, PropertyChangeEvent event);
}
