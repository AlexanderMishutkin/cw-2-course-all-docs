/*
 * @author apmishutkin@edu.hse.ru
 */

package events;

import data.structures.Updatable;
import data.structures.Wrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Realizes classical behavior of tree node
 */
public class UpdatableNode implements Wrapper, Updatable {
    private final Object object;
    private UpdatableNode parent = null;
    private final ArrayList<UpdatableNode> children = new ArrayList<>();
    Boolean changed = false;

    /**
     * Creates <code>Node</code> that has no children and no parent
     *
     * @param data to wrap
     */
    public UpdatableNode(Object data) {
        object = data;
    }

    /**
     * Creates <code>Node</code> that has no parent
     *
     * @param data     to wrap
     * @param children to add
     */
    public UpdatableNode(Object data, List<UpdatableNode> children) {
        this(data);
        setChildren(children);
    }

    /**
     * Creates <code>Node</code> that has no children
     *
     * @param data   to wrap
     * @param parent to add
     */
    public UpdatableNode(Object data, UpdatableNode parent) {
        this(data);
        setParent(parent);
    }

    public boolean hasParent() {
        return parent != null;
    }

    public UpdatableNode getParent() {
        return parent;
    }

    public boolean hasChildren() {
        return children.size() > 0;
    }

    /**
     * Check if some of children, grandchildren etc has such object as inner data
     *
     * @param object to look for in descendants
     */
    public boolean isMyDescendant(Object object) {
        if (object == getValue()) {
            return true;
        }
        return children.stream().anyMatch((UpdatableNode child) -> child.isMyDescendant(object));
    }

    /**
     * Check if some of parents, grandparents etc has same object as given node as inner data
     *
     * @param node to check for role parent / of grand parent
     */
    public boolean isMyAncestor(UpdatableNode node) {
        return node.isMyDescendant(getValue());
    }

    /**
     * @return data wrapped by this wrapper
     */
    @Override
    public Object getValue() {
        return object;
    }

    public List<UpdatableNode> getChildren() {
        return new ArrayList<>(children);
    }

    public void setChildren(List<UpdatableNode> children) {
        getChildren().forEach(this::removeChild);
        children.forEach(this::addChild);
    }

    public void setParent(UpdatableNode parent) {
        if (parent != null) {
            if (!parent.getChildren().contains(this)) {
                parent.removeChild(this);
            }
        }
        this.parent = parent;
        sync();
    }

    public void addChild(UpdatableNode child) {
        this.children.add(child);
        sync();
    }

    public void removeChild(UpdatableNode child) {
        this.children.remove(child);
        child.setParent(null);
    }

    public UpdatableNode getChild(Integer index) {
        return this.children.get(index);
    }

    public void setChild(Integer index, UpdatableNode newChild) {
        this.children.get(index).setParent(null);
        this.children.set(index, newChild);
        sync();
    }

    private void sync() {
        if (parent != null) {
            if (!parent.getChildren().contains(this)) {
                parent.addChild(this);
            }
        }
        children.forEach(child -> {
            if (child.parent != this) {
                child.setParent(this);
            }
        });
    }

    /**
     * Accept changes
     */
    @Override
    public void change() {
        if (changed) {
            return;
        }
        changed = true;
        if (hasParent()) {
            getParent().change();
        } else {
            EventCascade.registerRootNodeToUpdate(this);
        }
    }

    /**
     * Process accepted changes
     */
    @Override
    public void update() {
        changed = false; // TODO: Do I need to update children too?
    }

    /**
     * Is element changed and never updated after it?
     */
    @Override
    public boolean isChanged() {
        return changed;
    }
}
