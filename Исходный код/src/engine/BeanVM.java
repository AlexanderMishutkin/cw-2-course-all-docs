/*
 * @author apmishutkin@edu.hse.ru
 */

package engine;

import events.EventCascade;
import type.loader.TypeLoader;

/**
 * Methods that conduct work of all beans
 */
public class BeanVM {
    /**
     * clear caches of <code>TypeLoader</code>s, <code>EventCascade</code> and everywhere else
     */
    public static void clearAllCaches() {
        TypeLoader.clearCache();
        EventCascade.clearCache();
    }
}
