/*
 * @author apmishutkin@edu.hse.ru
 */

package type.hardcoded;

/**
 * Simple type created from common hardcoded java classes
 */
public class JavaClassType extends HardcodedType {
    /**
     * @param targetClass to create type from
     */
    public JavaClassType(Class<?> targetClass) {
        super(targetClass);
    }
}
