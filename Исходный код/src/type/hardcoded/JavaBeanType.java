/*
 * @author apmishutkin@edu.hse.ru
 */

package type.hardcoded;

/**
 * TODO: Add access level analysis
 */
public class JavaBeanType extends HardcodedType {
    public JavaBeanType(Class<?> targetClass) {
        super(targetClass);
    }
}
