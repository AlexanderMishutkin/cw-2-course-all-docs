/*
 * @author apmishutkin@edu.hse.ru
 */

package type.hardcoded;

import beans.Bean;
import beans.BeanFunctionalProvider;
import beans.HardcodedBeanFunctionalProvider;
import properties.PropertiesInfoProvider;

import java.lang.reflect.Modifier;

/**
 * Type that represents Bean VM Beans
 */
public class BeanVM_BeanType extends HardcodedType {
    /**
     * Creates Type from Bean via initializing it
     *
     * @param targetClass to create type from. Must be Bean VM bean
     */
    public BeanVM_BeanType(Class<?> targetClass) {
        super(targetClass);
    }

    /**
     * @return some object of target class
     */
    private Bean instantiateImplementation() {
        Bean protoBean = null;

        if (!Modifier.isAbstract(implementationClass.getModifiers())) {
            try {
                protoBean = (Bean) implementationClass.getDeclaredConstructor().newInstance();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return protoBean;
    }

    /**
     * For use in Bean constructor only
     */
    public BeanFunctionalProvider getBeanFunctionalProvider(Bean owner) {
        return new HardcodedBeanFunctionalProvider(this, owner);
    }

    @Override
    public void setUp() {
        super.setUp();
        this.propertiesInfoProvider = new PropertiesInfoProvider(instantiateImplementation());
    }
}
