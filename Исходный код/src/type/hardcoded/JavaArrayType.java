/*
 * @author apmishutkin@edu.hse.ru
 */

package type.hardcoded;

import type.Type;

/**
 * Type fpr common java array
 */
public class JavaArrayType extends JavaClassType {
    final Type elementType;

    /**
     * @param targetClass Java array
     */
    public JavaArrayType(Class<?> targetClass) {
        super(targetClass);
        elementType = Type.forClass(implementationClass.getComponentType());
    }

    /**
     * @return type of array element
     */
    public Type getElementType() {
        return elementType;
    }
}
