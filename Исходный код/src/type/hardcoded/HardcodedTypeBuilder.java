/*
 * @author apmishutkin@edu.hse.ru
 */

package type.hardcoded;

import beans.Bean;
import introspection.ObjectHelper;

/**
 * Creates type in two steps
 *
 * Firstly, creates link on empty <code>HardcodedType</code>
 * Than set this type
 *
 * first step is needed to register type in <code>TypeLoader</code>
 */
public class HardcodedTypeBuilder {
    private final HardcodedType hardcodedType;
    private boolean sufficientlyConfigured = false;

    /**
     * @param targetClass to build type from
     */
    public HardcodedTypeBuilder(Class<?> targetClass) {
        if (ObjectHelper.isInstantiable(targetClass)) { // check for the component...
            if (Bean.class.isAssignableFrom(targetClass)) // dealing with our beans...
            {
                hardcodedType = new BeanVM_BeanType(targetClass);
            } else {
                hardcodedType = new JavaBeanType(targetClass); // dealing with classic JavaBeans (i.e. all other beans)
            }
        } else if (targetClass.isArray()) { // any java array class:
            hardcodedType = new JavaArrayType(targetClass);
        } else { // any arbitrary java class:
            hardcodedType = new JavaClassType(targetClass);
        }
    }

    /**
     * @return finished type
     */
    public HardcodedType getHardcodedType() {
        if (!sufficientlyConfigured) {
            throw new IllegalStateException(
                    "Set up HardcodedType by giving class, before use. Or set flag allowUnfinished to 'True'");
        }
        return hardcodedType;
    }

    /**
     * @param allowUnfinished do we need empty not set up link?
     * @return type finished or not
     */
    public HardcodedType getHardcodedType(boolean allowUnfinished) {
        if (!allowUnfinished) {
            return getHardcodedType();
        }
        return hardcodedType;
    }

    /**
     * Finish type creation
     */
    public void create() {
        hardcodedType.setUp();
        sufficientlyConfigured = true;
    }
}
