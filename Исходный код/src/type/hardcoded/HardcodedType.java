/*
 * @author apmishutkin@edu.hse.ru
 */

package type.hardcoded;

import introspection.ObjectHelper;
import properties.PropertiesInfoProvider;
import type.Type;
import type.loader.TypeLoader;

/**
 * Type represents all hardcoded classes, Beans, Java Beans etc
 *
 * <code>TypeLoader</code> initialize type creation,
 * then type is being registered in <code>TypeLoader</code> and then
 * type set up is being finished. For controlling of this process <code>HardcodedTypeBuilder</code> is used
 */
public abstract class HardcodedType extends Type {
    protected Class<?> superClass;
    protected Type superType;
    protected final Class<?> implementationClass;
    protected boolean isNullAssignable;
    protected TypeLoader typeLoader;
    protected PropertiesInfoProvider propertiesInfoProvider;

    /**
     * Actually, just create link to register in <code>TypeLoader</code>
     * <code>HardcodedType</code> is created after calling <code>.setUp()</code>
     *
     * @param targetClass to create <code>Type</code> from
     */
    HardcodedType(Class<?> targetClass) {
        implementationClass = targetClass;
        propertiesInfoProvider = new PropertiesInfoProvider(targetClass);
    }

    /**
     * Finish type creation (after it is registered in <code>TypeLoader</code>)
     */
    public void setUp() {
        this.superClass = implementationClass.getSuperclass();
        this.superType = (superClass == null) ? null : Type.forClass(superClass);
        this.typeLoader = TypeLoader.forClass(implementationClass);
        this.isNullAssignable = Type.isNullable(implementationClass);
    }

    /**
     * @return implementation class superclass
     */
    public Class<?> getSuperClass() {
        return superClass;
    }

    /**
     * @return implementation classed used for creating this type
     */
    public Class<?> getImplementationClass() {
        return implementationClass;
    }

    /**
     * @return super type created from superclass of implementation class if it (superclass) exists
     */
    public Type getSuperType() {
        return superType;
    }

    /**
     * @return <code>TypeLoader</code> that initialized this type creation and register this type
     */
    public TypeLoader getTypeLoader() {
        return typeLoader;
    }

    /**
     * @param value value that we assign
     * @return can we assign value in current type variable?
     */
    @Override
    public boolean isValueAssignable(Object value) {
        if (value == null) {
            return isNullAssignable;
        }

        return ObjectHelper.isValueAssignable(value, implementationClass);
    }

    /**
     * @return provider of property info
     */
    public PropertiesInfoProvider getPropertiesInfoProvider() {
        return propertiesInfoProvider;
    }

    /**
     * @return actually, just class type was created from
     */
    @Override
    public String getName() {
        return implementationClass.getName();
    }
}
