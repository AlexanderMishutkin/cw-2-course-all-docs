/*
 * @author apmishutkin@edu.hse.ru
 */

package type.loader;

import type.Type;
import type.hardcoded.HardcodedTypeBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * Loads types for classes, java beans, beans and arrays
 * <p>
 * Default implementation is created via <code>forClass</code> method
 */
abstract public class TypeLoader {
    /**
     * The instance of TypeLoader that is used to load basic Java types (corresponds to Bootstrap ClassLoader).
     */
    private static TypeLoader BASE_TYPE_LOADER;

    /**
     * Classloader used by target class
     */
    final protected ClassLoader classLoader;

    /**
     * ClassLoader to TypeLoader Table
     */
    private static final Map<ClassLoader, TypeLoader> CLASS_L_2_TYPE_L = new HashMap<>();

    /**
     * Class to it's default value map.
     * It can be fulfilled not only in <code>TypeLoader</code>, but in <code>Type</code> to.
     */
    private static final Map<Class<?>, Object> DEFAULT_VALUES = new HashMap<>();

    /**
     * Creates <code>typeLoader</code> based on target class' <code>classLoader</code>
     *
     * @param classLoader that loads target class
     */
    protected TypeLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    /**
     * Creates <code>typeLoader</code> for target class by using it <code>classLoader</code>
     *
     * @param targetClass target class to turn into <code>Type</code>
     * @return <code>TypeLoader</code> that can load target class' type
     */
    synchronized public static TypeLoader forClass(Class<?> targetClass) {
        ClassLoader classLoader = targetClass.getClassLoader();
        return forClassLoader(classLoader);
    }

    /**
     * Creates <code>typeLoader</code> for target class by using it <code>classLoader</code>
     *
     * @param classLoader target class loader to turn into <code>TypeLoader</code>
     * @return <code>TypeLoader</code> that can load target class' type
     */
    synchronized protected static TypeLoader forClassLoader(ClassLoader classLoader) {
        if (classLoader == null) { // Primitive classes
            if (BASE_TYPE_LOADER == null) {
                BASE_TYPE_LOADER = new DefaultTypeLoader(null);
            }
            return BASE_TYPE_LOADER;
        }

        TypeLoader typeLoader = CLASS_L_2_TYPE_L.getOrDefault(classLoader, null); // Checking cache
        if (typeLoader == null) {
            typeLoader = new DefaultTypeLoader(classLoader);
            CLASS_L_2_TYPE_L.put(classLoader, typeLoader);
        }
        return typeLoader;
    }

    /**
     * Add default values for primitive classes. it is needed not to return <code>null</code> as int default value
     *
     * @param targetClass default value's owner class
     * @param value       default value
     */
    public static void registerDefaultValue(Class<?> targetClass, Object value) {
        DEFAULT_VALUES.put(targetClass, value);
    }

    /**
     * @param targetClass class expected to be primitive non nullable
     * @return true if class was registered as primitive type with non null default value
     */
    static public boolean isNullable(Class<?> targetClass) {
        return !DEFAULT_VALUES.containsKey(targetClass);
    }

    /**
     * Loads type
     * <p>
     * Actually checks for parent class loaders, than checks for cache and only after it all - loads class
     *
     * @param targetClass class to base loaded <code>Type</code> on
     * @return <code>Type</code> for target class
     */
    public Type getTypeFromClass(Class<?> targetClass) {
        Type result = null;

        TypeLoader parent = getParent();
        if (parent != null) {
            result = parent.getTypeFromClass(targetClass);
        }

        if (result == null) {
            result = loadType(targetClass);
        }

        return result;
    }

    /**
     * Loads type if target class has no parent and not being cached
     *
     * @param targetClass class to base loaded <code>Type</code> on
     * @return <code>Type</code> for target class
     */
    protected abstract Type loadType(Class<?> targetClass);

    /**
     * @return <code>TypeLoader</code> for parent class
     */
    public final TypeLoader getParent() {
        TypeLoader result = null;
        if (this.classLoader != null) {
            result = forClassLoader(classLoader.getParent());
        }
        return result;
    }

    /**
     * @param implementationClass class to get default value for
     * @return default value
     */
    public Object getRegisteredDefaultValue(Class<?> implementationClass) {
        return DEFAULT_VALUES.getOrDefault(implementationClass, null);
    }

    /**
     * Clear cache for all type loaders
     */
    public static void clearCache() {
        CLASS_L_2_TYPE_L.clear();
        DEFAULT_VALUES.clear();
        Type.registerDefaultTypes();
    }

    /**
     * Default type loader implementation. Actually delegates <code>loadType</code> method to <code>HardcodedTypeBuilder</code>
     */
    public static class DefaultTypeLoader extends TypeLoader {
        /**
         * Current <code>TypeLoader</code>'s cache. Also used to avoid looping during loading
         */
        private final Map<Class<?>, Type> class_2_type = new HashMap<>();

        /**
         * Creates <code>typeLoader</code> based on target class' <code>classLoader</code>
         *
         * @param classLoader that loads target class
         */
        public DefaultTypeLoader(ClassLoader classLoader) {
            super(classLoader);
        }

        /**
         * Loads type if target class has no parent and not being cached.
         * Actually delegates to <code>HardcodedTypeBuilder</code>
         *
         * @param targetClass class to base loaded <code>Type</code> on
         * @return <code>Type</code> for target class
         */
        @Override
        protected Type loadType(Class<?> targetClass) {
            if (targetClass == null) {
                return null;
            }
            Type result = class_2_type.getOrDefault(targetClass, null);
            if (result == null) {
                HardcodedTypeBuilder hardcodedTypeBuilder = new HardcodedTypeBuilder(targetClass);

                class_2_type.put(targetClass, hardcodedTypeBuilder.getHardcodedType(true));
                hardcodedTypeBuilder.create();

                result = hardcodedTypeBuilder.getHardcodedType();
            }
            return result;
        }
    }
}
