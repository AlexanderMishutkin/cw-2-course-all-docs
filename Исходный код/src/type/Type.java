/*
 * @author apmishutkin@edu.hse.ru
 */

package type;

import beans.Bean;
import beans.Instance;
import type.hardcoded.HardcodedType;
import type.hardcoded.JavaArrayType;
import type.loader.TypeLoader;

import java.lang.reflect.Array;

/**
 * Type of Bean VM object.
 * <p>
 * It is used for creating instances, crating typed Variables, some introspection and lots of other Bean VM inner logic.
 */
public abstract class Type extends Instance {
    private static final Type TYPE_OF_TYPE;

    public static void registerDefaultTypes() {
        setDefaultValue(Boolean.class, Boolean.FALSE);
        setDefaultValue(Character.class, '\u0000');
        setDefaultValue(Byte.class, (byte) 0);
        setDefaultValue(Short.class, (short) 0);
        setDefaultValue(Integer.class, 0);
        setDefaultValue(Long.class, 0L);
        setDefaultValue(Float.class, 0.0f);
        setDefaultValue(Double.class, 0.0d);
        setDefaultValue(String.class, "");

        setDefaultValue(boolean.class, Boolean.FALSE);
        setDefaultValue(char.class, '\u0000');
        setDefaultValue(byte.class, (byte) 0);
        setDefaultValue(short.class, (short) 0);
        setDefaultValue(int.class, 0);
        setDefaultValue(long.class, 0L);
        setDefaultValue(float.class, 0.0f);
        setDefaultValue(double.class, 0.0d);
    }

    static {
        registerDefaultTypes();

        TYPE_OF_TYPE = Type.forClass(Type.class);
    }

    /**
     * Returns type of objects of given class. Bean VM analog of <code>T.class</code> in java VM
     *
     * @param targetClass to get type of
     * @return type of class
     */
    public static Type forClass(Class<?> targetClass) {
        if (targetClass == null) {
            return null;
        }

        TypeLoader typeLoader = TypeLoader.forClass(targetClass);
        return typeLoader.getTypeFromClass(targetClass);
    }

    /**
     * Type of Type is constant object. Assume <code>t</code> variable of <code>Type</code>:
     * <pre>
     * Type.forClass(Type) == t.getType() == t.getType().getType()
     * </pre>
     *
     * @return type of type
     */
    @Override
    public Type getType() {
        return TYPE_OF_TYPE;
    }

    /**
     * Set default value for class in <code>TypeLoader</code>
     * Actually delegates to <code>TypeLoader.registerDefaultValue</code>
     *
     * @param targetClass default value's owner class
     * @param value       default value
     */
    static public void setDefaultValue(Class<?> targetClass, Object value) {
        TypeLoader.registerDefaultValue(targetClass, value);
    }

    /**
     * @param targetClass class expected to be primitive non nullable
     * @return true if class was registered as primitive type with non null default value
     */
    static public boolean isNullable(Class<?> targetClass) {
        return TypeLoader.isNullable(targetClass);
    }

    /**
     * @return short non formal name of type
     */
    public String getName() {
        // This code must never be executed
        throw new IllegalStateException("You can not create abstract type, those, you do not need it's name");
    }

    /**
     * Checks weather this type is a bean type
     *
     * @return true, is this type is a Bean type.
     */
    public boolean isBeanType() {
        if (this instanceof HardcodedType) {
            Class<?> c = ((HardcodedType) this).getImplementationClass();
            return Bean.class.isAssignableFrom(c);
        } else {
            return true;
        }
    }

    /**
     * Checks weather this type is an array of beans
     *
     * @return true, is this type is a Bean type array.
     */
    public boolean isBeanArrayType() {
        if (this instanceof JavaArrayType) {
            Type elementType = ((JavaArrayType) this).getElementType();
            return elementType.isBeanType();
        }
        return false;
    }

    /**
     * Checks weather this type is an array
     *
     * @return true, is this type is an array.
     */
    public boolean isArrayType() {
        return this instanceof JavaArrayType;
    }

    /**
     * @param value value that we assign
     * @return can we assign value in current type variable?
     */
    abstract public boolean isValueAssignable(Object value);


    /**
     * Just <code>DEFAULT_VALUES</code>' getter
     *
     * @return default value
     */
    public Object getDefaultValue() {
        if (this instanceof JavaArrayType) {
            Type elementType = ((JavaArrayType) this).getElementType();
            Class<?> elementClass = ((HardcodedType) elementType).getImplementationClass();
            return Array.newInstance(elementClass, 0);
        }
        if (this instanceof HardcodedType) {
            return ((HardcodedType) this).getTypeLoader()
                    .getRegisteredDefaultValue(((HardcodedType) this).getImplementationClass());
        } else {
            return null;
        }
    }


    /**
     * Just <code>DEFAULT_VALUES</code>' getter
     *
     * @param targetClass default value's owner class
     * @return default value
     */
    public static Object getDefaultValue(Class<?> targetClass) {
        Type type = Type.forClass(targetClass);
        return type.getDefaultValue();
    }
}
