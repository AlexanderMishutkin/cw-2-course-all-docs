/*
 * @author apmishutkin@edu.hse.ru
 */

package beans;

import type.Type;
import type.hardcoded.BeanVM_BeanType;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Bean VM bean
 * <p>
 * Extend this class to use your class as Bean VM bean
 * <p>
 * Override <code>update</code> method to track changes in your properties.
 * Override <code>propertyChange</code> method to use bean as event listener.
 * <code>get*, set*, add*, remove*</code> methods are standard bean API
 */
public class Bean extends Instance {
    final private BeanFunctionalProvider beanFunctionalProvider;

    public Bean() {
        beanFunctionalProvider = ((BeanVM_BeanType) Type.forClass(this.getClass())).getBeanFunctionalProvider(this);
    }

    /**
     * @return type of Bean VM object
     */
    @Override
    public Type getType() {
        return beanFunctionalProvider.getType();
    }

    /**
     * Initialize property with some not default value
     */
    protected void initPropertyValue(String propertyName, Object initValue) {
        beanFunctionalProvider.initPropertyValue(propertyName, initValue);
    }

    /**
     * Initialize property with some not default value
     */
    protected void initPropertyValue(int propertyIndex, Object initValue) {
        beanFunctionalProvider.initPropertyValue(propertyIndex, initValue);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public void setPropertyValue(String propertyName, Object propertyValue) {
        beanFunctionalProvider.setPropertyValue(propertyName, propertyValue);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public void setPropertyValue(int propertyIndex, Object propertyValue) {
        beanFunctionalProvider.setPropertyValue(propertyIndex, propertyValue);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public void setPropertyValue(String propertyName, int elementIndex, Object propertyValue) {
        beanFunctionalProvider.setPropertyValue(propertyName, elementIndex, propertyValue);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public void setPropertyValue(int propertyIndex, int elementIndex, Object propertyValue) {
        beanFunctionalProvider.setPropertyValue(propertyIndex, elementIndex, propertyValue);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected void sudoSetPropertyValue(String propertyName, Object propertyValue) {
        beanFunctionalProvider.sudoSetPropertyValue(propertyName, propertyValue);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected void sudoSetPropertyValue(int propertyIndex, Object propertyValue) {
        beanFunctionalProvider.sudoSetPropertyValue(propertyIndex, propertyValue);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected void sudoSetPropertyValue(String propertyName, int elementIndex, Object propertyValue) {
        beanFunctionalProvider.sudoSetPropertyValue(propertyName, elementIndex, propertyValue);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected void sudoSetPropertyValue(int propertyIndex, int elementIndex, Object propertyValue) {
        beanFunctionalProvider.sudoSetPropertyValue(propertyIndex, elementIndex, propertyValue);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public Object getPropertyValue(String propertyName) {
        return beanFunctionalProvider.getPropertyValue(propertyName);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public Object getPropertyValue(int propertyIndex) {
        return beanFunctionalProvider.getPropertyValue(propertyIndex);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public Object getPropertyValue(String propertyName, int elementIndex) {
        return beanFunctionalProvider.getPropertyValue(propertyName, elementIndex);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public Object getPropertyValue(int propertyIndex, int elementIndex) {
        return beanFunctionalProvider.getPropertyValue(propertyIndex, elementIndex);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected Object sudoGetPropertyValue(String propertyName) {
        return beanFunctionalProvider.sudoGetPropertyValue(propertyName);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected Object sudoGetPropertyValue(int propertyIndex) {
        return beanFunctionalProvider.sudoGetPropertyValue(propertyIndex);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected Object sudoGetPropertyValue(String propertyName, int elementIndex) {
        return beanFunctionalProvider.sudoGetPropertyValue(propertyName, elementIndex);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected Object sudoGetPropertyValue(int propertyIndex, int elementIndex) {
        return beanFunctionalProvider.sudoGetPropertyValue(propertyIndex, elementIndex);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        beanFunctionalProvider.addPropertyChangeListener(listener);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        beanFunctionalProvider.removePropertyChangeListener(listener);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        beanFunctionalProvider.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        beanFunctionalProvider.removePropertyChangeListener(propertyName, listener);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public void addPropertyChangeListener(int propertyIndex, PropertyChangeListener listener) {
        beanFunctionalProvider.addPropertyChangeListener(propertyIndex, listener);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    public void removePropertyChangeListener(int propertyIndex, PropertyChangeListener listener) {
        beanFunctionalProvider.removePropertyChangeListener(propertyIndex, listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected void sudoAddPropertyChangeListener(PropertyChangeListener listener) {
        beanFunctionalProvider.sudoAddPropertyChangeListener(listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected void sudoRemovePropertyChangeListener(PropertyChangeListener listener) {
        beanFunctionalProvider.sudoRemovePropertyChangeListener(listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected void sudoAddPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        beanFunctionalProvider.sudoAddPropertyChangeListener(propertyName, listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected void sudoRemovePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        beanFunctionalProvider.sudoRemovePropertyChangeListener(propertyName, listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected void sudoAddPropertyChangeListener(int propertyIndex, PropertyChangeListener listener) {
        beanFunctionalProvider.sudoAddPropertyChangeListener(propertyIndex, listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    protected void sudoRemovePropertyChangeListener(int propertyIndex, PropertyChangeListener listener) {
        beanFunctionalProvider.sudoRemovePropertyChangeListener(propertyIndex, listener);
    }

    /**
     * This method is called if this <code>Bean</code> was given as listener to some property and that property changed
     *
     * @param event that came from some bound property or bean
     */
    protected void propertyChange(PropertyChangeEvent event) {
    }

    /**
     * This method is called after all listeners handle some change and this bean or some of his properties was changed
     * during handling process
     */
    protected void update() {

    }
}
