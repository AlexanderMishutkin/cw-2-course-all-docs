/*
 * @author apmishutkin@edu.hse.ru
 */

package beans;

import events.NamedPropertyChangeListener;
import events.UpdatableNode;
import type.Type;

import java.beans.PropertyChangeListener;

/**
 * Provides all bean basic functionality - stores <code>Type</code> and properties.
 * Keeps <code>Variable</code>s structured as tree to implement correct update logic and <code>EventCascade</code>
 * pattern. Also implements event listening and subscribing
 */
public abstract class BeanFunctionalProvider extends UpdatableNode implements NamedPropertyChangeListener {
    public BeanFunctionalProvider(Object data) {
        super(data);
    }

    abstract protected Type getType();

    /**
     * Initialize property with some not default value
     */
    abstract protected void initPropertyValue(String propertyName, Object initValue);

    /**
     * Initialize property with some not default value
     */
    abstract protected void initPropertyValue(int propertyIndex, Object initValue);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public void setPropertyValue(String propertyName, Object propertyValue);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public void setPropertyValue(int propertyIndex, Object propertyValue);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public void setPropertyValue(String propertyName, int elementIndex, Object propertyValue);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public void setPropertyValue(int propertyIndex, int elementIndex, Object propertyValue);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected void sudoSetPropertyValue(String propertyName, Object propertyValue);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected void sudoSetPropertyValue(int propertyIndex, Object propertyValue);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected void sudoSetPropertyValue(String propertyName, int elementIndex, Object propertyValue);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected void sudoSetPropertyValue(int propertyIndex, int elementIndex, Object propertyValue);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public Object getPropertyValue(String propertyName);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public Object getPropertyValue(int propertyIndex);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public Object getPropertyValue(String propertyName, int elementIndex);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public Object getPropertyValue(int propertyIndex, int elementIndex);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected Object sudoGetPropertyValue(String propertyName);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected Object sudoGetPropertyValue(int propertyIndex);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected Object sudoGetPropertyValue(String propertyName, int elementIndex);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected Object sudoGetPropertyValue(int propertyIndex, int elementIndex);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public void removePropertyChangeListener(PropertyChangeListener listener);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public void addPropertyChangeListener(int propertyIndex, PropertyChangeListener listener);

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws exceptions.BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    abstract public void removePropertyChangeListener(int propertyIndex, PropertyChangeListener listener);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected void sudoAddPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected void sudoRemovePropertyChangeListener(PropertyChangeListener listener);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected void sudoAddPropertyChangeListener(String propertyName, PropertyChangeListener listener);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected void sudoRemovePropertyChangeListener(String propertyName, PropertyChangeListener listener);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected void sudoAddPropertyChangeListener(int propertyIndex, PropertyChangeListener listener);

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    abstract protected void sudoRemovePropertyChangeListener(int propertyIndex, PropertyChangeListener listener);
}
