/*
 * @author apmishutkin@edu.hse.ru
 */

package beans;

import events.EventCascade;
import events.UpdatableNode;
import exceptions.BeanVM_AccessLevelException;
import properties.HardcodedProperty;
import properties.PropertyInfo;
import sensetive.memory.Variable;
import type.Type;
import type.hardcoded.HardcodedType;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Provides all bean basic functionality for hardcoded types, also stores <code>Type</code> and properties
 * <p>
 * Keeps <code>Variable</code>s structured as tree to implement correct update logic and <code>EventCascade</code>
 * pattern. Also implements event listening and subscribing
 */
public class HardcodedBeanFunctionalProvider extends BeanFunctionalProvider {
    protected final HardcodedType type;
    protected final Bean owner;
    private final ArrayList<PropertyChangeListener> listeners;

    public HardcodedBeanFunctionalProvider(HardcodedType hardcodedType, Bean owner) {
        super(owner);
        type = hardcodedType;
        this.owner = owner;
        listeners = new ArrayList<>();
        implementBeanProperties();
    }

    public void implementBeanProperties() {
        PropertyInfo[] propertiesInfo = type.getPropertiesInfoProvider().getPropertiesInfo();
        HardcodedProperty[] properties = new HardcodedProperty[propertiesInfo.length];
        for (int i = 0; i < propertiesInfo.length; i++) {
            PropertyInfo propertyInfo = propertiesInfo[i];
            Type valueType = propertyInfo.getType();
            HardcodedProperty property = HardcodedProperty.fromPropertyInfo(this, propertyInfo);
            properties[i] = property;
        }
        setChildren(Arrays.asList(properties));
    }

    protected HardcodedProperty getProperty(int index) {
        return (HardcodedProperty) getChildren().get(index);
    }

    protected int getPropertyIndex(String propertyName) {
        List<UpdatableNode> children = getChildren();
        for (int i = 0, childrenSize = children.size(); i < childrenSize; i++) {
            UpdatableNode property = children.get(i);
            if (((HardcodedProperty) property).getName().equals(propertyName)) {
                return i;
            }
        }
        throw new IndexOutOfBoundsException(propertyName + " not in children list!");
    }

    @Override
    protected HardcodedType getType() {
        return type;
    }

    /**
     * Initialize property with some not default value
     */
    @Override
    protected void initPropertyValue(String propertyName, Object initValue) {
        initPropertyValue(getPropertyIndex(propertyName), initValue);
    }

    /**
     * Initialize property with some not default value
     */
    @Override
    protected void initPropertyValue(int propertyIndex, Object initValue) {
        getProperty(propertyIndex).logIn(this).sudoSetValue(initValue);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    public void setPropertyValue(String propertyName, Object propertyValue) {
        setPropertyValue(getPropertyIndex(propertyName), propertyValue);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    public void setPropertyValue(int propertyIndex, Object propertyValue) {
        getProperty(propertyIndex).setValue(propertyValue);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    public void setPropertyValue(String propertyName, int elementIndex, Object propertyValue) {
        setPropertyValue(getPropertyIndex(propertyName), elementIndex, propertyValue);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be mutable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    public void setPropertyValue(int propertyIndex, int elementIndex, Object propertyValue) {
        getProperty(propertyIndex).setValue(elementIndex, propertyValue);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    protected void sudoSetPropertyValue(String propertyName, Object propertyValue) {
        sudoSetPropertyValue(getPropertyIndex(propertyName), propertyValue);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    protected void sudoSetPropertyValue(int propertyIndex, Object propertyValue) {
        getProperty(propertyIndex).logIn(this).sudoSetValue(propertyValue);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    protected void sudoSetPropertyValue(String propertyName, int elementIndex, Object propertyValue) {
        sudoSetPropertyValue(getPropertyIndex(propertyName), elementIndex, propertyValue);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    protected void sudoSetPropertyValue(int propertyIndex, int elementIndex, Object propertyValue) {
        getProperty(propertyIndex).logIn(this).sudoSetValue(elementIndex, propertyValue);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    public Object getPropertyValue(String propertyName) {
        return getPropertyValue(getPropertyIndex(propertyName));
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    public Object getPropertyValue(int propertyIndex) {
        return getProperty(propertyIndex).getValue();
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    public Object getPropertyValue(String propertyName, int elementIndex) {
        return getPropertyValue(getPropertyIndex(propertyName), elementIndex);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be readable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    public Object getPropertyValue(int propertyIndex, int elementIndex) {
        return getProperty(propertyIndex).getValue(elementIndex);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    protected Object sudoGetPropertyValue(String propertyName) {
        return sudoGetPropertyValue(getPropertyIndex(propertyName));
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    protected Object sudoGetPropertyValue(int propertyIndex) {
        return getProperty(propertyIndex).logIn(this).sudoGetValue();
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    protected Object sudoGetPropertyValue(String propertyName, int elementIndex) {
        return sudoGetPropertyValue(getPropertyIndex(propertyName), elementIndex);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    protected Object sudoGetPropertyValue(int propertyIndex, int elementIndex) {
        return getProperty(propertyIndex).logIn(this).sudoGetValue(elementIndex);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    synchronized public void addPropertyChangeListener(PropertyChangeListener listener) {
        listeners.add(listener);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    synchronized public void removePropertyChangeListener(PropertyChangeListener listener) {
        listeners.remove(listener);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    synchronized public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        addPropertyChangeListener(getPropertyIndex(propertyName), listener);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    synchronized public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        removePropertyChangeListener(getPropertyIndex(propertyName), listener);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    synchronized public void addPropertyChangeListener(int propertyIndex, PropertyChangeListener listener) {
        getProperty(propertyIndex).addPropertyChangeListener(listener);
    }

    /**
     * Classical bean operation, implemented without any reflection. Checks level of access
     * <p>
     * Property must be bindable!
     *
     * @throws BeanVM_AccessLevelException if this operation is prohibited on current property
     */
    @Override
    synchronized public void removePropertyChangeListener(int propertyIndex, PropertyChangeListener listener) {
        getProperty(propertyIndex).addPropertyChangeListener(listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    synchronized protected void sudoAddPropertyChangeListener(PropertyChangeListener listener) {
        addPropertyChangeListener(listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    synchronized protected void sudoRemovePropertyChangeListener(PropertyChangeListener listener) {
        removePropertyChangeListener(listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    synchronized protected void sudoAddPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        sudoAddPropertyChangeListener(getPropertyIndex(propertyName), listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    synchronized protected void sudoRemovePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        sudoRemovePropertyChangeListener(getPropertyIndex(propertyName), listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    synchronized protected void sudoAddPropertyChangeListener(int propertyIndex, PropertyChangeListener listener) {
        getProperty(propertyIndex).logIn(this).sudoAddPropertyChangeListener(listener);
    }

    /**
     * Classical bean operation, but without any access check. For internal use only
     */
    @Override
    synchronized protected void sudoRemovePropertyChangeListener(int propertyIndex, PropertyChangeListener listener) {
        getProperty(propertyIndex).logIn(this).sudoRemovePropertyChangeListener(listener);
    }

    @Override
    public void propertyChange(String propertyName, PropertyChangeEvent event) {
        PropertyChangeEvent recreated = EventCascade.recreatePropertyChangeEventFromExisting(event,
                (Variable) event.getSource(), propertyName);
        propertyChange(recreated);
        for (PropertyChangeListener listener : listeners) {
            listener.propertyChange(recreated);
        }
        EventCascade.eventDelivered(recreated);
    }

    /**
     * This method gets called when a bound property is changed.
     *
     * @param evt A PropertyChangeEvent object describing the event source
     *            and the property that has changed.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        change();
        ((Bean) getValue()).propertyChange(evt);
    }
}
