/*
 * @author apmishutkin@edu.hse.ru
 */

package beans;

import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;

/**
 * Hack to hide Bean API setters from Bean introspection
 */
public class BeanBeanInfo extends SimpleBeanInfo {
    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        return new PropertyDescriptor[]{};
    }
}