/*
 * @author apmishutkin@edu.hse.ru
 */

package beans;

import log.Loggable;
import type.Type;

/**
 * Methods that are must-have for all Bean VM objects
 */
public abstract class Instance extends Loggable {
    /**
     * @return type of Bean VM object
     */
    abstract public Type getType();
}
